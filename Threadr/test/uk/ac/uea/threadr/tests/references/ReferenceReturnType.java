package uk.ac.uea.threadr.tests.references;

import uk.ac.uea.threadr.AbstractReturnType;

/**
 * A reference class that implements the {@link AbstractReturnType} interface 
 * to be used in the unit tests for the Threadr framework. The return type is 
 * typed to an array of integers.
 * 
 * @author Jordan Woerner
 */
public class ReferenceReturnType implements AbstractReturnType<int[]> {
	
	/** Serialisation identifier. */
	private static final long serialVersionUID = 1L;
	/** The data to return. */
	private int[] data = null;

	@Override
	public void setResult(int[] data) {
		
		this.data = data;
	}

	@Override
	public int[] getResult() {
		
		return data;
	}
}