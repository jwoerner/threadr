/**
 * Holds classes related to the thread safety testing of classes.
 *  
 * @author Jordan Woerner
 */
package uk.ac.uea.threadr.internal.testing;