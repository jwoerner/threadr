package uk.ac.uea.threadr.internal.vmhandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.concurrent.Callable;
import java.util.logging.Level;

import uk.ac.uea.threadr.AbstractReturnType;
import uk.ac.uea.threadr.ParallelTask;
import uk.ac.uea.threadr.Threadr;

/**
 * Creates and monitors a new instance of the Java Virtual Machine (JVM) whilst
 *  it executes a {@link ParallelTask} provided to this wrapper.
 * 
 * @author Jordan Woerner
 * @version 1.1
 */
public class VMWrapper implements Callable<AbstractReturnType<?>> {

	/** The ParallelTask to send to a new Virtual Machine. */
	private ParallelTask task;
	/** The hash code for the ParallelTask to be processed. */
	private int taskHash;
	/** A description of the new Java VM process to run. */
	private ProcessBuilder process;
	/** Has the task finished executing? */
	private boolean executed;
	/** The result from executing the new JVM. */
	private AbstractReturnType<?> result;
	
	/**
	 * Creates a new Java Virtual Machine instance for the specified 
	 * {@link ParallelTask} task when the {@link VMWrapper#call()} method 
	 * is executed.
	 * 
	 * @param task The ParallelTask to execute in a new JVM instance.
	 * @since 1.0
	 */
	public VMWrapper(ParallelTask task) {
		
		this.task = task;
		this.taskHash = this.task.hashCode();
		this.executed = false;
		
		/* Set up the new JVM process. */
		String fileSeperator = System.getProperty("file.separator");
		String classpath = System.getProperty("java.class.path");
		String path = String.format("%s%sbin%sjava", 
				System.getProperty("java.home"), fileSeperator, 
				fileSeperator, classpath);
		
		this.process = new ProcessBuilder(path,
				"-cp", classpath, 
				"uk.ac.uea.threadr.internal.vmhandler.VMInstance", 
				String.valueOf(this.taskHash));
		
		process.inheritIO(); // Redirect the new VM I/O.
	}
	
	/**
	 * Creates a new Java Virtual Machine instance for the specified 
	 * {@link ParallelTask} task when the {@link VMWrapper#call()} method 
	 * is executed. Custom parameters can be passed to the new VM instance if 
	 * required by passing each parameter as a new argument.
	 * 
	 * @param task The ParallelTask to execute in a new JVM instance.
	 * @param args The custom parameters for this VM as Strings.
	 * @since 1.0
	 */
	public VMWrapper(ParallelTask task, String...args) {
		
		this.task = task;
		this.taskHash = this.task.hashCode();
		this.executed = false;
		
		/* Set up the new JVM process. */
		String fileSeperator = System.getProperty("file.separator");
		String classpath = System.getProperty("java.class.path");
		String path = String.format("%s%sbin%sjava", 
				System.getProperty("java.home"), fileSeperator, 
				fileSeperator, classpath);
		
		StringBuilder custArgs = new StringBuilder();
		for (int i = 0; i < args.length; i++) {
			custArgs.append(args[i]);
			if (i < args.length-1) {
				custArgs.append(" ");
			}
		}
		
		this.process = new ProcessBuilder(path, custArgs.toString(),
				"-cp", classpath, 
				"uk.ac.uea.threadr.internal.vmhandler.VMInstance", 
				String.valueOf(this.taskHash));
		
		process.inheritIO(); // Redirect the new VM I/O.
	}
	
	/**
	 * Prepares the task to be executed in a new JVM instance by creating deep 
	 * copies of the objects used for the task and serialising them to be read 
	 * by the new JVM using the {@link VMInstance} class.
	 * 
	 * @throws IOException Thrown if the serialisation fails or the file that 
	 * the task is stored in cannot be written.
	 * @since 1.0
	 */
	private void prepareTask() throws IOException {
		
		/* Create the File and Streams for the temporary file. */
		String fileSep = System.getProperty("file.separator");
		String tempDir = System.getProperty("java.io.tmpdir");
		String fileDir = String.format("%s%s", tempDir, 
				fileSep);
		String fileName = String.format("%s-in.tmp", taskHash);
		File resultFile = new File(fileDir, fileName);
		resultFile.deleteOnExit();
		
		OutputStream oStream = null;
		ObjectOutputStream objStrm = null;
		
		try {
			/* Set up the OutputStreams to store the object. */
			oStream = new FileOutputStream(resultFile);
			objStrm = new ObjectOutputStream(oStream);
			
			objStrm.writeObject(task); // Write the object.
		} catch (FileNotFoundException fileEx) {
			throw new IOException(String.format("Could not write file %s\n"
					+ "Reason: %s\"", fileName, fileEx.getMessage()));
		} catch (NotSerializableException nsEx) {
			throw new IOException(String.format("Could not serialise task "
					+ "'%s'", task.getClass().getName()));
		} finally {
			/* Attempt to close the ObjectOutputStream if it was created. */
			if (objStrm != null) {
				try {
					objStrm.close();
				} catch (IOException ioEx) {
					throw new IOException(String.format("Could not close the "
							+ "ObjectOutputStream for task %s", 
							task.getClass().getName()));
				}
			}
			/* Attempt to close the OutputStream used if it was created. */
			if (oStream != null) {
				try {
					oStream.close();
				} catch (IOException ioEx) {
					throw new IOException(String.format("Could not close the "
							+ "OutputStream for task %s", 
							task.getClass().getName()));
				}
			}
		}
	}
	
	/**
	 * Retrieves the result from a completed JVM execution.
	 * 
	 * @throws IOException Thrown if the temporary file cannot be accessed.
	 * @throws ClassNotFoundException If the class being de-serialised cannot 
	 * be loaded this will throw.
	 * @since 1.0
	 */
	private void retreiveResult() throws IOException, ClassNotFoundException {
		
		/* Set up the File and Streams to retrieve the result. */
		String fileSep = System.getProperty("file.separator");
		String tempDir = System.getProperty("java.io.tmpdir");
		String fileDir = String.format("%s%s", tempDir, 
				fileSep);
		String fileName = String.format("%s-out.tmp", taskHash);
		File resultFile = new File(fileDir, fileName);
		resultFile.deleteOnExit();
		
		InputStream inStrm = null;
		ObjectInputStream objStrm = null;
		
		try {
			inStrm = new FileInputStream(resultFile);
			objStrm = new ObjectInputStream(inStrm);
			result = (AbstractReturnType<?>)objStrm.readObject();
		} catch (ClassNotFoundException cfnEx) {
			throw new ClassNotFoundException(String.format("Could not find "
					+ "class 'AbstractReturnType' whilst retriving results for"
					+ " %s!\nReason: %s", task.getClass().getName(), 
					cfnEx.getMessage()));
		} catch (IOException ioEx) {
			throw new IOException(String.format("Could not access file '%s'\n"
					+ "Reason: %s", resultFile.getName(), ioEx.getMessage()));
		} finally {
			/* Attempt to close the ObjectInputStream safely. */
			if (objStrm != null) {
				try {
					objStrm.close();
				} catch (IOException ioex) {
					Threadr.logger.warning("Failed to close ObjectInputStream!");
				} finally {
					/* Ensure it is always null in the end. */
					objStrm = null;
				}
			}
			/* Try to close the InputStream used safely. */
			if (inStrm != null) {
				try {
					inStrm.close();
				} catch (IOException ioex) {
					Threadr.logger.warning("Failed to close InputStream!");
				} finally {
					inStrm = null;
				}
			}
		}
	}

	/**
	 * Sets up a new JVM process and monitors it. If the process exits cleanly 
	 * (exit code 0), the result is retrieved and returned.
	 * 
	 * @return The result of a {@link ParallelTask} executing in a new JVM 
	 * instance. Returns null if any critical errors are generated during 
	 * execution.
	 * @throws Exception Thrown if there are any problems during the storing of
	 *  tasks or the retrieval of results.
	 * @since 1.0
	 */
	@Override
	public AbstractReturnType<?> call() throws Exception {
		
		try {
			prepareTask();
		} catch (IOException ioEx) {
			Threadr.logger.severe(ioEx.getMessage());
			if (Threadr.logger.getLevel().intValue() <= Level.FINE.intValue())
				ioEx.printStackTrace();
			throw ioEx;
		}
		/* Start the new VM. */
		Process runningProc = process.start();
		runningProc.waitFor();
		this.executed = true;
		
		/* If execution completed safely, get the result. */
		if (runningProc.exitValue() == 0) {
			try {
				retreiveResult(); // Get the result first.
			} catch (Exception ex) {
				Threadr.logger.severe(ex.getMessage());
				if (Threadr.logger.getLevel().intValue() <= Level.FINE.intValue())
					ex.printStackTrace();
				throw ex;
			}
			return result;
		}
		
		return null; // Don't return anything if there was an execution error.
	}
	
	/**
	 * Compares this VMWrapper against the provided Object.
	 * 
	 * @return Returns true if this VMWrapper and the provided Object are both 
	 * of the same type and all the member values are the same. Returns false 
	 * if the provided Object is null, cannot be cast to VMWrapper, or has 
	 * differences in the members.
	 * @since 1.1
	 */
	@Override
	public boolean equals(Object obj) {
		
		VMWrapper other = null;
		
		try {
			other = (VMWrapper)obj;
		} catch (ClassCastException cast) {
			Threadr.logger.finest("Could not cast parameter to VMWrapper for "
					+ "comprison.");
			return false;
		} catch (NullPointerException npe) {
			Threadr.logger.finest("VMWrapper equals(Object) parameter null.");
			return false;
		}
		
		/* Compare the members of the two VMWrapper instances. */
		if (this.taskHash != other.taskHash) return false;
		if (!this.task.equals(other.task)) return false;
		
		return true; // Only return true if everything matches.
	}
	
	/**
	 * Gets information about this VMWrapper such as the name of the task 
	 * provided, if there is a result held and if the process has finished 
	 * executing.
	 * 
	 * @return Returns details for this VMWrapper as a String.
	 * @since 1.1
	 */
	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("Task name: ").append(task.getClass().getName());
		sb.append("\tResult stored: ").append(result == null ? "Yes" : "No");
		sb.append("\tProcess executed: ").append(executed ? "Yes" : "No");
		
		return sb.toString();
	}
}
