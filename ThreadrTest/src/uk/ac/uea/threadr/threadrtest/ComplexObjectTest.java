package uk.ac.uea.threadr.threadrtest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;

import uk.ac.uea.threadr.AbstractReturnType;
import uk.ac.uea.threadr.ParallelTask;
import uk.ac.uea.threadr.Threadr;

public class ComplexObjectTest {

	private List<ComplexObject> objects;
	
	public ComplexObjectTest() {
		objects = new ArrayList<>();
		for (int i = 0; i < ThreadrTest.SIZE; i++) {
			objects.add(new ComplexObject());
		}
	}
	
	public List<ComplexObject> getObjects() {
		return this.objects;
	}

	private static class ComplexObject implements Comparable<ComplexObject>, ParallelTask, Serializable {
		
		private static final long serialVersionUID = -2398049924999261255L;
		private double id;
		private String title;
	
		public ComplexObject() {
			
			Random rand = new Random();
			id = rand.nextDouble();
			byte[] randomBytes = new byte[8];
			rand.nextBytes(randomBytes);
			title = new String(randomBytes);
		}
	
		@Override
		public int compareTo(ComplexObject o) {
			
			if (this.id < o.id)
				return -1;
			
			if (this.id > o.id)
				return 1;
						
			return 0;
		}
		
		@Override
		public AbstractReturnType<?> call() {
			
			ComplexReturnType result = new ComplexReturnType();
			
			Threadr.logger.fine(title);
			result.addResult(this);
			
			return result;
		}
	}
	
	private static class ComplexReturnType implements AbstractReturnType<List<ComplexObject>> {
		
		private static final long serialVersionUID = -8263232825557543479L;
		private List<ComplexObject> objects;
		
		public ComplexReturnType() {
			objects = new ArrayList<>();
		}
		
		public void addResult(ComplexObject result) {
			objects.add(result);
		}

		@Override
		public void setResult(List<ComplexObject> result) {
			objects = result;
		}

		@Override
		public List<ComplexObject> getResult() {
			return objects;
		}
	}
	
	public static void main(String[] args) {
		
		ComplexObjectTest test = new ComplexObjectTest();
		Threadr framework = new Threadr(test.objects);
		framework.enableLogging();
		framework.setOutputLevel(Level.FINE);
		framework.execute();
		Threadr.logger.info("Done");
	}
}
