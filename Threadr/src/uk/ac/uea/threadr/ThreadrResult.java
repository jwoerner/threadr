package uk.ac.uea.threadr;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Holds the results of a Threadr API {@link Threadr#execute()} execution. 
 * Maps the {@link ParallelTask} that was executed to the  
 * {@link AbstractReturnType} resulting from the execution.
 * 
 * @author Jordan Woerner
 * @version 1.2
 */
public final class ThreadrResult {
	
	/** A mapping of the tasks to their results. */
	private Map<ParallelTask, AbstractReturnType<?>> results;
	
	/**
	 * Creates a new instance of ThreadrResult ready to accept task and 
	 * result instances.
	 * 
	 * @since 1.0
	 */
	public ThreadrResult() {
		
		results = new HashMap<>();
	}
	
	/**
	 * Adds the specified task to this ThreadrResult instance if it is not 
	 * already contained inside.
	 * 
	 * @param task The {@link ParallelTask} instance to add to this 
	 * ThreadrResult.
	 * @return Returns true if the task was added to this ThreadrResult, false 
	 * otherwise.
	 * @since 1.0
	 */
	public synchronized boolean addTask(ParallelTask task) {
		
		if (!results.containsKey(task)) {
			results.put(task, null);
			return true;
		}
		
		return false;
	}
	
	/**
	 * Adds the provided result to this ThreadrResult using the specified task 
	 * as the index.
	 * 
	 * @param task The {@link ParallelTask} instance to map this result to.
	 * @param result The {@link AbstractReturnType} result to map to the task.
	 * @return Returns an {@link AbstractReturnType} if one was already mapped 
	 * to the task used as the key. Otherwise returns null.
	 * @since 1.0
	 */
	public synchronized AbstractReturnType<?> addResult(ParallelTask task, AbstractReturnType<?> result) {
		
		return results.put(task, result);
	}
	
	/**
	 * Removes a result and the task mapped to it from this ThreadrResult. The 
	 * result mapped to the tasks is returned if one existed.
	 * 
	 * @param task The {@link ParallelTask} to remove from this ThreadrResult.
	 * @return The result from executing the {@link ParallelTask} if one 
	 * existed, null otherwise.
	 * @since 1.2
	 */
	public synchronized AbstractReturnType<?> removeResult(ParallelTask task) {
		
		return results.remove(task);
	}
	
	/**
	 * Gets the {@link AbstractReturnType} result mapped to the specified 
	 * {@link ParallelTask} instance.
	 * 
	 * @param task The {@link ParallelTask} to use as the key for this mapping.
	 * @return Returns an {@link AbstractReturnType} if there is a mapping in 
	 * this ThreadrResult. Null if no mapping is found.
	 * @since 1.0
	 */
	public AbstractReturnType<?> getResult(ParallelTask task) {
		
		return results.get(task);
	}
	
	/**
	 * Gets the tasks held in this ThreadrResult.
	 * 
	 * @return A {@link Set} of objects that implement the {@link ParallelTask}
	 *  interface.
	 * @since 1.0
	 */
	public Set<? extends ParallelTask> getTasks() {
		
		return results.keySet();
	}
	
	/**
	 * Gets the results held in this ThreadrResult.
	 * 
	 * @return A {@link Collection} of objects that implement the 
	 * {@link AbstractReturnType} interface.
	 * @since 1.0
	 */
	public Collection<AbstractReturnType<?>> getResults() {
		
		return results.values();
	}
	
	/**
	 * Combines this ThreadrResult with another ThreadrResult object. New keys 
	 * will be added. Existing keys will be overwritten.
	 *  
	 * @param other The ThreadrResult to combine this with.
	 * @since 1.1
	 */
	public void combine(ThreadrResult other) {
		
		combine(other, true);
	}
	
	/**
	 * Combines this ThreadrResult with another ThreadrResult object. New keys 
	 * will be added. If specified, existing keys will have their values 
	 * overwritten.
	 *  
	 * @param other The ThreadrResult to combine this with.
	 * @param overwrite Set to true to overwrite existing results stored in 
	 * this ThreadrResult.
	 * @since 1.1
	 */
	public void combine(ThreadrResult other, boolean overwrite) {
				
		/* Otherwise, loop through the other ThreadrResult. */
		for (Object key : other.getTasks()) {
			ParallelTask task = (ParallelTask)key;
			AbstractReturnType<?> value = other.getResult(task);
			if (!results.containsKey(key)) {
				/* Only add keys that do not exist. */
				results.put(task, value);
			} else if (overwrite) {
				/* Overwrite all the existing entries if needed. */
				results.put(task, value);
			}
		}
	}
	
	/**
	 * Gets the number of tasks held in this ThreadrResult instance.
	 * 
	 * @return The number of tasks held in this instance as an integer.
	 * @since 1.0
	 */
	public int size() {
		
		return results.size();
	}
	
	/**
	 * Empties this ThreadrResult, removing all stored tasks and results.
	 */
	public void clear() {
		
		results.clear();
	}
	
	/**
	 * Displays some details and the number of entries in this ThreadResult.
	 * 
	 * @return The details of this ThreadrResult as a String.
	 * @since 1.2
	 */
	@Override
	public String toString() {
		
		return "ThreadrResult - Size: " + size();
	}
}
