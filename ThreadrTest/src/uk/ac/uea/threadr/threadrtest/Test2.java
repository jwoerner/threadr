package uk.ac.uea.threadr.threadrtest;

import java.io.Serializable;
import java.util.Random;

import uk.ac.uea.threadr.AbstractReturnType;
import uk.ac.uea.threadr.ParallelTask;
import uk.ac.uea.threadr.internal.MemoryMonitor;
import uk.ac.uea.threadr.threadrtest.ThreadrTest.MemoryReturnType;

public class Test2 implements ParallelTask, Serializable {
	
	private static final long serialVersionUID = 1L;
	private int[] values;
	public static final int SIZE = ThreadrTest.SIZE;
	private static final Random random = new Random();

	public Test2() {
		
		values = new int[SIZE];
		for (int i = 0; i < SIZE; i++) {
			values[i] = random.nextInt(1000000);
		}
	}
	
	@Override
	public AbstractReturnType<?> call() {
		
		MemoryReturnType result = new MemoryReturnType();
		
		boolean sorted = false;
		while(!sorted) {
			sorted = true;
			for (int i = 1; i < SIZE; i++) {
				if (values[i-1] > values[i]) {
					int tmp = values[i];
					values[i] = values[i - 1];
					values[i - 1] = tmp;
					sorted = false;
				}
			}
		}
		result.setResult(MemoryMonitor.getInstance());
		return result;
	}
	
	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder("Pre: ");
		for (int i = 0; i < 10; i++) {
			sb.append(values[i]).append(',');
		}
		
		return sb.toString();
	}
}
