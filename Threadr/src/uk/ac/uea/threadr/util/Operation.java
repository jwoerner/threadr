package uk.ac.uea.threadr.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * Stores a constructor, method or field access from the provided Class to 
 * be used when creating new instances of the class. Abstract implementation to
 *  allow for polymorphic storage of each Operation type.
 *  
 * @author Jordan Woerner
 * @version 1.3
 */
public abstract class Operation {
	
	/**
	 * Describes the various access modifiers in Java.
	 * 
	 * @author Jordan Woerner
	 * @version 1.0
	 * @since 1.3
	 */
	public enum Access {
		PUBLIC,
		PACKAGE,
		PROTECTED,
		PRIVATE;
	}
	
	/** The Class type this Operation is performed on. */
	protected Class<?> type;
	
	/** The access modifier for this operation. */
	protected Access access;
	
	/** Is the operation an array type? */
	protected boolean isArray;
	/** Is the operation a static member? */
	protected boolean isStatic;
	/** Is the operation a final member? */
	protected boolean isFinal;
	/** Is the operation an abstract declaration? */
	protected boolean isAbstract;
	/** Is the operation volatile? */
	protected boolean isVolatile;
	/** Is the operation synchronised? */
	protected boolean isSynchronised;
	/** Is the operation a constructor? */
	protected boolean isConstructor;
	/** Is the operation a method? */
	protected boolean isMethod;
	/** Is the operation a field access? */
	protected boolean isField;
	
	/** The return type of this operation. */
	protected Class<?> returnType;
	
	/** An array of all the types this operation uses as parameters. */
	protected Class<?>[] paramTypes;
	
	/** An array of all the Annotations attached to this Operation. */
	protected Annotation[] annotations;
	
	/** The name of this operation. */
	protected String name;
	/** The signature of this operation. */
	protected String signature;
	
	/**
	 * Gets the type this Operation is from.
	 * 
	 * @return The type of this Operation as a Class.
	 * @since 1.0
	 */
	public final Class<?> getType() { return this.type; }
	
	/**
	 * Gets the access modifier for this Operation. Access can be PUBLIC, 
	 * PACKAGE, PROTECTED or PRIVATE.
	 * 
	 * @return The access modifier as a constant from the {@link Access} enum.
	 * @since 1.3
	 */
	public final Access getAccessModifier() { return this.access; } 
	
	/**
	 * Gets whether or not this Operation returns an array type. The array 
	 * type will not be reflected in the {@link Operation#getReturnType()} 
	 * method, so calling this method is required if you need to check for 
	 * array return types.
	 * 
	 * @return Returns true if this Operation returns an array type, 
	 * false otherwise.
	 * @since 1.2
	 */
	public final boolean isArray() { return this.isArray; }	
	
	/**
	 * Gets whether or not this Operation is a static operation on the class.
	 * 
	 * @return Returns true if this Operation is static, false otherwise.
	 * @since 1.0
	 */
	public final boolean isStatic() { return this.isStatic; }
	
	/**
	 * Checks if this Operation is a final operation or not.
	 * 
	 * @return Returns true if this Operation is final. False otherwise.
	 * @since 1.1
	 */
	public final boolean isFinal() { return this.isFinal; }
	
	/**
	 * Checks if this Operation is an abstract declaration or not.
	 * 
	 * @return Returns true if this Operation has the abstract modifier. False 
	 * otherwise.
	 * @since 1.1
	 */
	public final boolean isAbstract() { return this.isAbstract; }
	
	/**
	 * Checks if this Operation is an volatile operation or not.
	 * 
	 * @return Returns true if this Operation has the volatile modifier. False 
	 * otherwise.
	 * @since 1.2
	 */
	public final boolean isVolatile() { return this.isVolatile; }
	
	/**
	 * Checks to see if this Operation is synchronised or not.
	 * 
	 * @return Returns true if this Operation is synchronised. False otherwise.
	 * @since 1.2
	 */
	public final boolean isSynchronised() { return this.isSynchronised; }
	
	/**
	 * Gets whether or not this Operation is a constructor.
	 * 
	 * @return Returns true if this is a constructor, false otherwise.
	 * @since 1.0
	 */
	public final boolean isConstructor() { return this.isConstructor; }
	
	/**
	 * Get whether or not this Operation is a method.
	 * 
	 * @return Returns true if this is a method, false otherwise.
	 * @since 1.0
	 */
	public final boolean isMethod() { return this.isMethod; }
	
	/**
	 * Gets whether or not this Operation is a field access.
	 * 
	 * @return Returns true if this is a field access, false otherwise.
	 * @since 1.0
	 */
	public final boolean isFieldAccess() { return this.isField; }
	
	/**
	 * Gets the return type of this Operation.
	 * 
	 * @return The return type as a Class object.
	 * @since 1.0
	 */
	public final Class<?> getReturnType() { return this.returnType; }
	
	/**
	 * Gets the parameter types used by this Operation.
	 * 
	 * @return The parameter types as an array of Class objects.
	 * @since 1.0
	 */
	public final Class<?>[] getParamTypes() { return this.paramTypes; }
	
	/**
	 * Gets the name of this Operation.
	 * 
	 * @return The name of the Operation as a String.
	 * @since 1.0
	 */
	public final String getName() { return this.name; }
	
	/**
	 * Gets an array of the {@link Annotation}s attached to this Operation.
	 * 
	 * @return An array of Annotation types.
	 * @since 1.1
	 */
	public final Annotation[] getAnnotations() { return annotations; }
	
	/**
	 * Checks if this Operation contains the specified {@link Annotation}.
	 * 
	 * @param annotation The Annotation to compare against as a Class of the 
	 * Annotation.
	 * @return Returns true if the Operation uses the Annotation, false 
	 * otherwise.
	 * @since 1.1
	 */
	public final boolean hasAnnotation(Class<? extends Annotation> annotation)
	{	
		for (Annotation a : annotations) {
			if (a.annotationType() == annotation) return true;
		}
		return false;
	}
	
	/**
	 * Runs this Operation.
	 * 
	 * @return An Object referencing the result of execution.
	 * @throws Exception Thrown should anything go wrong during the execution 
	 * of the {@link Operation}. Will not catch exceptions thrown by the 
	 * Operation if they are not propagated up the stack by the Operation.
	 * @since 1.0
	 */
	public abstract Object execute(Object obj, Object[] args) 
			throws Exception;
	
	/**
	 * Prints the signature of this operation.
	 * 
	 * @since 1.0
	 */
	@Override
	public final String toString() { return signature; }
	
	/**
	 * Stores a Constructor from the specified Class to be used when creating 
	 * new instances of Classes.
	 * 
	 * @author Jordan Woerner
	 * @version 1.0
	 */
	public static final class ConstructorOperation extends Operation {

		/** The Constructor this Operation uses. */
		private Constructor<?> constructor;
		
		/**
		 * Creates a new ConstructorOperation for the specified Class.
		 * 
		 * @param clazz The class this Constructor is from.
		 * @param constr The Constructor to store.
		 * @since 1.0
		 */
		public ConstructorOperation(final Class<?> clazz, 
				final Constructor<?> constr) {
			
			this.type = clazz;
			this.isConstructor = true;
			
			/* Work out which access modifier is correct for this Operation. */
			if (Modifier.isPublic(constr.getModifiers())) {
				this.access = Access.PUBLIC;
			} else if (Modifier.isProtected(constr.getModifiers())) {
				this.access = Access.PROTECTED;
			} else if (Modifier.isPrivate(constr.getModifiers())) {
				this.access = Access.PRIVATE;
			} else {
				this.access = Access.PACKAGE;
			}
			
			this.isArray = false;
			this.isStatic = false;
			this.isFinal = false;
			this.isAbstract = false;
			this.isVolatile = false;
			this.isSynchronised = false;
			
			this.paramTypes = constr.getParameterTypes();
			this.annotations = constr.getAnnotations();
			
			this.name = constr.getName();
			this.signature = constr.toGenericString();
			this.returnType = clazz;
			
			this.constructor = constr;
			this.constructor.setAccessible(true); // Force public.
		}

		/**
		 * Executes this ConstructorOperation.
		 * 
		 * @return An Object referencing the result of execution.
		 * @throws NullPointerException Thrown if the constructor cannot be 
		 * invoked for any reason, the class is abstract or the invocation 
		 * causes an OutOfMemoryError.
		 * @since 1.0
		 */
		@Override
		public Object execute(final Object obj, final Object[] args) 
				throws Exception {

			/* If the Object is already constructed, just use that. */
			if (obj != null) return obj;

			try {
				/* Invoke the constructor with the provided arguments. */
				return constructor.newInstance(args);
			} catch (IllegalAccessException iaEx) {
				throw new Exception(String.format("Cannot access constructor '%s' in class '%s'\n", 
						constructor.toGenericString()));
			} catch (IllegalArgumentException iargEx) {
				throw new Exception(String.format("Illegal argument provided to constructor '%s'\nReason - %s", 
						constructor.toGenericString(), iargEx.getMessage()));
			} catch (InstantiationException instEx) {
				throw new Exception(String.format("Failed to instantiate object using constructor '%s'\n", 
						constructor.toGenericString()));
			} catch (InvocationTargetException invEx) {
				throw new Exception(String.format("Could not invoke constructor '%s'\nReason: %s", 
						constructor.toGenericString(), invEx.getMessage()));
			} catch (OutOfMemoryError memEx) {
				throw new Exception(String.format("Out of memory instantiating '%s'!\n", 
						constructor.getDeclaringClass().getCanonicalName()));
			} catch (NullPointerException npEx) {
				throw new Exception(String.format("Constructor '%s' returned null\n", 
						constructor.toGenericString()));
			} catch (Exception ex) {
				throw new Exception(String.format("Misc exception occured executing constructor '%s'\nReason - %s\n", 
						constructor.getName(), ex.getMessage()));
			}
		}
	}
	
	/**
	 * Stores a Method from the specified Class to be used when creating new 
	 * instances of Classes.
	 * 
	 * @author Jordan Woerner
	 * @version 1.1
	 */
	public static final class MethodOperation extends Operation {

		/** The method this Operation uses. */
		private Method method;
		
		/**
		 * Creates a new MethodOperation for the specified Class.
		 * 
		 * @param clazz The Class this Method belongs to.
		 * @param mthd The Method to store.
		 * @since 1.0
		 */
		public MethodOperation(final Class<?> clazz, final Method mthd) {
			
			this.type = clazz;
			this.isMethod = true;
			
			/* Work out which access modifier is correct for this Operation. */
			if (Modifier.isPublic(mthd.getModifiers())) {
				this.access = Access.PUBLIC;
			} else if (Modifier.isProtected(mthd.getModifiers())) {
				this.access = Access.PROTECTED;
			} else if (Modifier.isPrivate(mthd.getModifiers())) {
				this.access = Access.PRIVATE;
			} else {
				this.access = Access.PACKAGE;
			}
			
			this.isArray = mthd.getReturnType().isArray();
			this.isStatic = Modifier.isStatic(mthd.getModifiers());
			this.isFinal = Modifier.isFinal(mthd.getModifiers());
			this.isAbstract = Modifier.isAbstract(mthd.getModifiers());
			this.isVolatile = false;
			this.isSynchronised = Modifier.isSynchronized(mthd.getModifiers());
			
			if (this.isArray) {
				this.returnType = mthd.getReturnType().getComponentType();
			} else {
				this.returnType = mthd.getReturnType();
			}
			
			this.paramTypes = mthd.getParameterTypes();
			this.annotations = mthd.getAnnotations();
			
			this.name = mthd.getName();
			this.signature = mthd.toGenericString();
			
			this.method = mthd;
			this.method.setAccessible(true); // Force public.
		}

		/**
		 * Executes this MethodOperation.
		 * 
		 * @return An Object referencing the result of execution.
		 * @throws NullPointerException Thrown if the method cannot be invoked 
		 * for any reason or the invocation causes an OutOfMemoryError.
		 * @since 1.0
		 */
		@Override
		public Object execute(final Object obj, final Object[] args) 
				throws Exception {

			/* If the object is null and this isn't a static method, return null. */
			if (obj == null && !this.isStatic) return null;
			
			try {
				/* Invoke the method. */
				if (method.getReturnType().equals(Void.TYPE)) {
					/* Do not assign result if the return type is void. */
					method.invoke(obj, args);
					return null;
				} else {
					/* Return the result otherwise. */
					return method.invoke(obj, args);
				}
			} catch (IllegalAccessException iaEx) {
				throw new Exception(String.format("Cannot access method '%s' in class '%s'\n", 
						method.toGenericString(), method.getDeclaringClass().getCanonicalName()));
			} catch (IllegalArgumentException iargEx) {
				throw new Exception(String.format("Illegal argument provided to method '%s' in class '%s'\nReason - %s", 
						method.toGenericString(), method.getDeclaringClass().getCanonicalName(), iargEx.getMessage()));
			} catch (InvocationTargetException invEx) {
				throw new Exception(String.format("Could not invoke method '%s' in class '%s'\nReason: %s\n", 
						method.toGenericString(), method.getDeclaringClass().getCanonicalName(), invEx.getMessage()));
			} catch (OutOfMemoryError memEx) {
				throw new Exception(String.format("Out of memory running method '%s' in class '%s'", 
						method.toGenericString(), method.getDeclaringClass().getCanonicalName()));
			} catch (NullPointerException npEx) {
				throw new Exception(String.format("Method '%s' in class '%s' returned null\n", 
						method.toGenericString(), method.getDeclaringClass().getCanonicalName()));
			} catch (Exception ex) {
				throw new Exception(String.format("Misc exception occured executing method '%s' in class '%s'\nReason - %s\n", 
						method.getName(), method.getDeclaringClass().getCanonicalName(), ex.getMessage()));
			}
		}
	}
	
	/**
	 * Stores a field access in the specified class to use when creating new 
	 * instances of Classes.
	 * 
	 * @author Jordan Woerner
	 * @version 1.1
	 */
	public static final class FieldOperation extends Operation {

		/** The Field this Operation uses. */
		private Field field;
		
		/**
		 * Creates a new FieldOperation for the specified Class.
		 * 
		 * @param clazz The Class this operation is performed on.
		 * @param fld The Field to store.
		 * @since 1.0
		 */
		public FieldOperation(final Class<?> clazz, final Field fld) {
			
			this.type = clazz;
			this.isField = true;
			
			/* Work out which access modifier is correct for this Operation. */
			if (Modifier.isPublic(fld.getModifiers())) {
				this.access = Access.PUBLIC;
			} else if (Modifier.isProtected(fld.getModifiers())) {
				this.access = Access.PROTECTED;
			} else if (Modifier.isPrivate(fld.getModifiers())) {
				this.access = Access.PRIVATE;
			} else {
				this.access = Access.PACKAGE;
			}
			
			this.isArray = fld.getType().isArray();
			this.isStatic = Modifier.isStatic(fld.getModifiers());
			this.isFinal = Modifier.isFinal(fld.getModifiers());
			this.isAbstract = Modifier.isAbstract(fld.getModifiers());
			this.isVolatile = Modifier.isVolatile(fld.getModifiers());
			this.isSynchronised = false;
			
			if (this.isArray) {
				this.returnType = fld.getType().getComponentType();
			} else {
				this.returnType = fld.getType();
			}
			this.paramTypes = null;
			this.annotations = fld.getAnnotations();
			
			this.name = fld.getName();
			this.signature = fld.toString();
			
			this.field = fld;
		}

		/**
		 * Executes this FieldOperation.
		 * 
		 * @return An Object referencing the result of execution.
		 * @throws NullPointerException Thrown if the value of the field cannot
		 *  be retrieved for any reason.
		 * @since 1.0
		 */
		@Override
		public Object execute(final Object obj, final Object[] args) 
				throws Exception {
			
			/* Return null if the object is null or this field isn't static. */
			if (obj == null && !this.isStatic) return null;
			
			try {
				/* Get the value held in this field by 'obj'. */
				return field.get(obj);
			} catch (IllegalAccessException iaEx) {
				throw new Exception(String.format("Cannot access field '%s' in class '%s'\n", field.getName(), 
						field.getDeclaringClass().getCanonicalName()));
			} catch (IllegalArgumentException iargEx) {
				throw new Exception(String.format("Field type mismatch! (Field '%s' of type %s in class '%s' cannot be assigned)\n", 
						field.getName(), field.getGenericType(), field.getDeclaringClass().getCanonicalName()));
			} catch (NullPointerException nEx) {
				throw new Exception(String.format("Field '%s' in class '%s' is null\n", 
						field.getName(), field.getDeclaringClass().getCanonicalName()));
			} catch (Exception ex) {
				throw new Exception(String.format("Misc exception occured accessing field '%s' in class '%s'\nReason - %s\n", 
						field.getName(), field.getDeclaringClass().getCanonicalName(), ex.getMessage()));
			}
		}
	}
}