package uk.ac.uea.threadr;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Ignores tasks that are sequential. These tasks will never be executed.
 * 
 * @author Jordan Woerner
 * @version 1.1
 */
public class IgnoreSequential implements SequentialHandler {

	/** Allows for logging information from this handler. */
	private static Logger logger = Threadr.logger;
	/** Contains all the tasks ignored by this handler. */
	private List<ParallelTask> tasks;
	
	/**
	 * Creates a new IgnoreSequential.
	 * 
	 * @since 1.0
	 */
	public IgnoreSequential() {
		
		tasks = new ArrayList<>();
	}
	
	/**
	 * @see SequentialHandler#getTasks()
	 * @since 1.1
	 */
	@Override
	public Set<? extends ParallelTask> getTasks() {
		
		return new HashSet<ParallelTask>(tasks);
	}
	
	/**
	 * @see SequentialHandler#handle(ParallelTask)
	 * @since 1.0
	 */
	@Override
	public AbstractReturnType<?> handle(ParallelTask task) {
		
		logger.fine(String.format("Ignoring task '%s' (Sequential.", 
				task.getClass().getName()));
		return null;
	}

	/**
	 * @see SequentialHandler#postExecution()
	 * @since 1.0
	 */
	@Override
	public ThreadrResult postExecution() {
		return null;
	}
	
	/**
	 * Gets the list of ignored tasks as a String.
	 * 
	 * @return The list of ignored ParallelTasks as a String.
	 * @since 1.1
	 */
	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		sb.append("Ignored tasks:\n");
		
		Iterator<?> it = tasks.iterator();
		while (it.hasNext()) {
			sb.append("\t").append(it.getClass().getName()).append("\n");
		}
		
		return sb.toString();
	}
}
