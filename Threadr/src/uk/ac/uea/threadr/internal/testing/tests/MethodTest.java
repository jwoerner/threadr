package uk.ac.uea.threadr.internal.testing.tests;

import uk.ac.uea.threadr.ThreadSafety;
import uk.ac.uea.threadr.ThreadTest;
import uk.ac.uea.threadr.util.ClassOperations;
import uk.ac.uea.threadr.util.Operation;
import uk.ac.uea.threadr.util.Operation.Access;
import uk.ac.uea.threadr.util.Operation.FieldOperation;
import uk.ac.uea.threadr.util.Operation.MethodOperation;

/**
 * Tests the methods of a class to see if they are all synchronised.
 * This was intended to be used, however I cannot figure out a decent scenario 
 * that it makes sense in.
 * 
 * @author Jordan Woerner
 * @version 1.0
 */
public class MethodTest implements ThreadTest {

	/**
	 * Tests the provided class methods to see if they are all synchronised or 
	 * not.
	 * 
	 * @see ThreadTest#test(Class)
	 * @since 1.0
	 */
	@Override
	public ThreadSafety test(final Class<?> clazz) {
		
		FieldOperation[] fields = ClassOperations.getFields(clazz);
		MethodOperation[] operations = ClassOperations.getMethods(clazz);
		
		/* Check all the fields in the class. */
		for (Operation o : fields) {
			if (o.getAccessModifier() == Access.PRIVATE
					&& o.isStatic() 
					&& !(o.isVolatile() || o.isFinal())) {
				/* If a field can't be accessed by other things, and is mutable
				    static, test the methods of this class to see if they are  
				   synchronised. */
				for (Operation m : operations) {
					if (!m.isSynchronised()) {
						return ThreadSafety.VM; // If one isn't, give up.
					}
				}
				break;
			}
		}
		
		return ThreadSafety.THREAD; // If nothing breaks, it's totally safe.
	}

}
