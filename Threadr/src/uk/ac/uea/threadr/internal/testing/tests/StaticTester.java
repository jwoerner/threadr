package uk.ac.uea.threadr.internal.testing.tests;

import java.util.ArrayList;
import java.util.List;

import uk.ac.uea.threadr.ThreadSafety;
import uk.ac.uea.threadr.ThreadSafe;
import uk.ac.uea.threadr.ThreadTest;
import uk.ac.uea.threadr.Threadr;
import uk.ac.uea.threadr.util.ClassOperations;
import uk.ac.uea.threadr.util.Operation.FieldOperation;

/**
 * Tests a provided class to see if it contains any mutable static members. 
 * These are a likely source of concurrency problems, so the class can be 
 * assumed thread unsafe, but VM safe if it contains these.
 * 
 * Since Version 1.1 this will ignore all fields that are marked with the 
 * {@link ThreadSafe} Annotation.
 * 
 * @author Jordan Woerner
 * @version 1.1
 */
public class StaticTester implements ThreadTest {
	
	/**
	 * Check all the fields and member variables of a class to ensure that 
	 * there are no non-final static fields. Ignores all fields marked with 
	 * the {@link ThreadSafe} Annotation.
	 * 
	 * @see ThreadTest#test(Class)
	 * @since 1.0
	 */
	@Override
	public ThreadSafety test(final Class<?> clazz) {
		
		List<FieldOperation> fields = new ArrayList<>();
		
		/* Get the fields from this class and it's superclass. */
		FieldOperation[] ownFields = ClassOperations.getFields(clazz);
		
		/* Add any fields that are static and mutable to the list. */
		for (FieldOperation f : ownFields) {
			if (f.hasAnnotation(ThreadSafe.class)) continue;
			
			/* Check for certain modifiers on the field we are looking at. */
			if (f.isStatic()) { // Static is a thread safety issue.
				/* Check to see if the type is Atomic or ThreadLocal. */
				if (f.getType().getName().matches("[\\w\\.]*Atomic[\\w]+\\.class")
					|| f.getType().equals(ThreadLocal.class))
					continue;
				/* Check the modifiers of the field. */
				if (!f.isFinal() && !f.isVolatile())
					fields.add(f);
			}
		}
		
		/* If the list is empty, the class passes. */
		ThreadSafety storedResult = fields.isEmpty() ?
				ThreadSafety.THREAD : ThreadSafety.VM;
		
		/* Get the super class for this class if it has one. */
		Class<?> superClass = clazz.getSuperclass();
		if (superClass != null) {
			/* If a super class exists, test it. */
			Threadr.logger.info(superClass.getName());
			ThreadSafety result = new StaticTester().test(superClass);
			/* Overwrite the stored value if it is less safe. */
			if (result.val() < storedResult.val()) {
				storedResult = result;
			}
		}
		
		return storedResult;
	}
}