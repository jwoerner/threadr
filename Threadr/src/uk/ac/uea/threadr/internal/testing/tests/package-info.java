/**
 * Holds some default tests for the framework to use.
 * @author Jordan Woerner
 */
package uk.ac.uea.threadr.internal.testing.tests;