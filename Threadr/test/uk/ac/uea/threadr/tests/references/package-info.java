/**
 * A collection of classes used in the Threadr unit tests.
 * 
 * @author Jordan Woerner
 */
package uk.ac.uea.threadr.tests.references;