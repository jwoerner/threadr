package uk.ac.uea.threadr;

/**
 * Describes a test for thread safety to be run when the framework executes.
 * 
 * @author Jordan Woerner
 * @version 1.0
 */
public interface ThreadTest {

	/**
	 * The test to execute on a class.
	 * 
	 * @param clazz The Class to test for thread safeness.
	 * @return Returns a ThreadSafety constant based on the thread safety of the 
	 * object being tested. Returns {@link ThreadSafety#VM} for objects that are 
	 * virtual machine safe, {@link ThreadSafety#THREAD} for objects that are 
	 * thread safe and {@link ThreadSafety#SEQUENTIAL} for objects that are 
	 * unsafe. Can return null if the result will not affect the safeness.
	 * @since 1.0
	 */
	public ThreadSafety test(final Class<?> clazz);
}