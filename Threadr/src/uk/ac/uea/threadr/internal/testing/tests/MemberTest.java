package uk.ac.uea.threadr.internal.testing.tests;

import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.logging.Logger;

import uk.ac.uea.threadr.ThreadSafety;
import uk.ac.uea.threadr.ThreadSafe;
import uk.ac.uea.threadr.ThreadTest;
import uk.ac.uea.threadr.Threadr;
import uk.ac.uea.threadr.util.ClassOperations;
import uk.ac.uea.threadr.util.Operation.Access;
import uk.ac.uea.threadr.util.Operation.FieldOperation;

/**
 * Tests all non-static members of the provided class to ensure that there are 
 * no thread safety violations within their implementations. 
 * 
 * @author Jordan Woerner
 * @version 1.0
 */
public class MemberTest implements ThreadTest {

	/** Allow logging of recursive tests. */
	private static final Logger logger = Threadr.logger;
	
	/**
	 * Tests the safety of the non-static members of the provided class.
	 * 
	 * @see ThreadTest#test(Class)
	 * @since 1.0
	 */
	@Override
	public ThreadSafety test(final Class<?> clazz) {
		
		ThreadSafety storedResult = ThreadSafety.THREAD;
		
		/* Get the fields from this class and it's superclass. */
		FieldOperation[] ownFields = ClassOperations.getFields(clazz);
		
		/* Add any fields that are non-static to the list. */
		for (FieldOperation f : ownFields) {
			/* Skip members marked with the 'ThreadSafe' annotation. */
			if (f.hasAnnotation(ThreadSafe.class)) continue;
			
			/* Check for certain modifiers on the field we are looking at. */
			if (!f.isStatic()) { // We only want to test non-static members.
				/* Check to see if the type is Atomic or ThreadLocal. */
				if (f.getReturnType().getName().matches("[\\w\\d\\.]*Atomic+[\\w\\d]*")
					|| f.getReturnType().equals(AtomicReference.class)
					|| f.getReturnType().equals(AtomicReferenceArray.class)
					|| f.getReturnType().equals(ThreadLocal.class))
						continue;
				/* If a field can be accessed by other classes in any way, it 
				   might not be thread safe. Though if it is final or volatile 
				   it should be fine. */
				if (f.getAccessModifier() != Access.PRIVATE
						&& !(f.isFinal() || f.isVolatile())) {
					return ThreadSafety.VM;
				}
				logger.fine(f.toString());
				/* Test the type of the field to see if it is unsafe. */
				ThreadSafety result = new MemberTest().test(f.getReturnType());
				if (result.val() < storedResult.val())
					storedResult = result;
			}
		}
		
		/* Get the super class for this class if it has one. */
		Class<?> superClass = null;
		if ((superClass = clazz.getSuperclass()) != null) {
			logger.finer(String.format("\tTesting class %s as field of %s", 
					superClass.getName(), clazz.getName()));
			/* If a super class exists, test it. */
			ThreadSafety result = new MemberTest().test(superClass);
			/* Overwrite the stored value if it is less safe. */
			if (result.val() < storedResult.val()) {
				logger.finer(String.format("\t%s unsafe.", 
						superClass.getName()));
				storedResult = result;
			}
		}
		
		return storedResult;
	}
}