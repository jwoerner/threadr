/**
 * A few utility classes to save repetitive complex code.
 * 
 * @author Jordan Woerner
 */
package uk.ac.uea.threadr.util;