/**
 * Provides the necessary classes to interact with the Threadr application and 
 * to handle output from Threadr threaded applications.
 * 
 * @author Jordan Woerner
 */
package uk.ac.uea.threadr;