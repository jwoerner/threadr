package uk.ac.uea.threadr;

import java.util.concurrent.Callable;

/**
 * Classes that wish to be used in the parallelisation framework must extend 
 * this class and override the method 'run()' to define their threaded action.
 * 
 * <p>ParallelTask is an extension of the Callable interface, allowing use in 
 * the Java ExecutorService framework.</p>
 * 
 * @author Jordan Woerner
 * @version 1.0
 */
public interface ParallelTask extends Callable<AbstractReturnType<?>> {
	
	/**
	 * Method must be overridden by extending class to enable support from 
	 * parallelisation framework.
	 * 
	 * @return The result from execution as an AbstractReturnType.
	 * @since 1.0
	 */
	@Override
	public AbstractReturnType<?> call();
}