package uk.ac.uea.threadr.internal;

import java.io.Serializable;
import java.lang.management.ManagementFactory;
import java.util.logging.Logger;

/**
 * Monitors the memory usage in a JVM. Serializable since version 1.1.
 * 
 * @author Jordan Woerner
 * @version 1.4
 */
public class MemoryMonitor implements Runnable, Serializable {

	/** Serialisation identifier. */
	private static final long serialVersionUID = -84844951572619164L;
	/** The Threadr logger used to log information and errors. */
	private static final Logger logger = Logger.getLogger("uk.ac.uea.threadr");
	
	/**
	 * Represents various memory units (B, KB, MB, etc.) in a more user 
	 * friendly manner.
	 * 
	 * @author Jordan Woerner
	 * @since 1.2
	 * @version 1.0
	 */
	public enum MemoryUnits {
		
		/** 
		 * The memory in bytes (x1).
		 * 
		 *  @since 1.0
		 */
		BYTE(1),
		/** 
		 * The memory in kilobytes (x1024).
		 * 
		 *  @since 1.0
		 */
		KILOBYTE(1024),
		/** 
		 * The memory in megabytes (x1024<sup>2</sup>).
		 * 
		 *  @since 1.0
		 */
		MEGABYTE(1024 * 1024),
		/** 
		 * The memory in gigabytes (x1024<sup>3</sup>).
		 * 
		 *  @since 1.0
		 */
		GIGABYTE(1024 * (1024 * 1024));
		
		/** The size of the unit in bytes. */
		private int val;
		
		/**
		 * Create an instance of a MemoryUnit and set the size.
		 * 
		 * @param size The size of the MemoryUnit as an integer.
		 * @since 1.0
		 */
		private MemoryUnits(int size) {
			val = size;
		}
		
		/**
		 * Get the size of the unit to multiply a value by.
		 * 
		 * @return The size of this unit as an integer.
		 * @since 1.0
		 */
		public int getSize() {
			return val;
		}
	}
	
	/** A per-VM instance of MemoryMonitor. */
	private static volatile MemoryMonitor instance = null;
	
	/** The process ID for the JVM this MemoryMonitor is running in. */
	private String id;
	/** The total memory reserved by the JVM. */
	private volatile long totalMemory;
	/** The total memory used by the JVM. */
	private volatile long maxUsed;
	/** The Runtime to check memory usage through. */
	private transient Runtime runtime;
	
	/**
	 * Sets up a new MemoryMonitor instance.
	 * 
	 * @since 1.0
	 */
	private MemoryMonitor() {
		
		this.runtime = Runtime.getRuntime();
		this.id = ManagementFactory.getRuntimeMXBean().getName();
		this.totalMemory = 0L;
		this.maxUsed = 0L;
	}
	
	/**
	 * Gets the stored MemoryMonitor instance for this JVM or creates one if 
	 * one is not found.
	 * 
	 * @return The MemoryMonitor for this JVM.
	 * @since 1.0
	 */
	public static MemoryMonitor getInstance() {
		
		if (instance == null) {
			instance = new MemoryMonitor();
		}
		
		return instance;
	}

	/**
	 * Starts this MemoryMonitor tracking the memory usage.
	 * 
	 * @since 1.0
	 */
	@Override
	public void run() {
		
		try {
			Thread.sleep(100);
		} catch (InterruptedException iEx) {
			logger.warning("Memory monitor thread interrupted");
		}
				
		totalMemory = runtime.totalMemory();
		long freeMemory = runtime.freeMemory();
		long used = totalMemory - freeMemory;
		
		if (used > maxUsed) {
			maxUsed = used;
		}
	}
	
	/**
	 * Gets the process ID for the JVM instance this {@link MemoryMonitor} is 
	 * executing within.
	 * 
	 * @return Returns the process ID for this MemoryMonitor as a String.
	 * @since 1.4
	 */
	public String getProcessID() {
		
		return this.id;
	}
	
	/**
	 * Gets the total memory reserved by the JVM in bytes.
	 * 
	 * @param unit The unit to retrieve the total memory used as a 
	 * {@link MemoryUnits} enumeration value.
	 * @return The memory reserved as a double.
	 * @since 1.1
	 */
	public double getTotalMemoryReserved(MemoryUnits unit) {
		
		return (double)totalMemory / unit.getSize();
	}
	
	/**
	 * Gets the maximum memory usage for this JVM in bytes.
	 * 
	 * @param unit The unit to retrieve the maximum memory used as a  
	 * {@link MemoryUnits} enumeration value.
	 * @return The memory usage as a double.
	 * @since 1.1
	 */
	public double getMaxUsedMemory(MemoryUnits unit) {
		
		return (double)maxUsed / unit.getSize();
	}
	
	/**
	 * Clears the stored values in this MemoryMonitor.
	 * 
	 * @since 1.0
	 */
	public void clear() {
		
		maxUsed = 0L;
		totalMemory = 0L;
	}
	
	/**
	 * Compares this MemoryMonitor against the provided object to see if they 
	 * are equal.
	 * 
	 * @return Returns true if this MemoryMonitor and the provided Object are 
	 * of the same type and have the same values stored in their members. 
	 * Returns false otherwise.
	 * @see Object#equals(Object)
	 * @since 1.4
	 */
	@Override
	public boolean equals(Object o) {
		
		MemoryMonitor other = null;
		/* Attempt to cast the provided object to MemoryMonitor. */
		try {
			other = (MemoryMonitor)o; 
		} catch (ClassCastException ex) {
			logger.finest("Could not cast argument to compare against "
					+ "MemoryMonitor.");
			return false;
		} catch (NullPointerException nex) {
			/* No null objects allowed. */
			logger.finest("Provided Object could not be compared against "
					+ "MemoryMonitor as it was null");
			return false;
		}
		
		/* Compare all the important members. */
		if (this.id != other.id) return false;
		if (this.maxUsed != other.maxUsed) return false;
		if (this.totalMemory != other.totalMemory) return false;
		
		return true; // the objects are the same.
	}
	
	/**
	 * Gets the memory usage details for this {@link MemoryMonitor} and returns
	 *  them as a String.
	 * 
	 * @return The memory usage details as a String.
	 * @since 1.3
	 */
	@Override
	public String toString() {
		
		return String.format("PID %s\tReserved memory: %.2fMB\tUsed memory: %.2fMB",
				this.id,
				getTotalMemoryReserved(MemoryUnits.MEGABYTE),
				getMaxUsedMemory(MemoryUnits.MEGABYTE));
	}
}
