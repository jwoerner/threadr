package uk.ac.uea.threadr;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Executes any provided ParallelTasks after Threadr is done executing the 
 * concurrent-safe tasks.
 * 
 * @author Jordan Woerner
 * @version 1.1
 */
public class PostSequentialHandler implements SequentialHandler {

	/** A List of the ParallelTasks to execute. */
	List<ParallelTask> tasks;
	
	/**
	 * Creates a new PostSequentialHandler that will execute tasks after 
	 * Threadr is done processing the concurrent tasks.
	 * 
	 * @since 1.0
	 */
	public PostSequentialHandler() {
		
		this.tasks = new ArrayList<>();
	}
	
	/**
	 * @see SequentialHandler#getTasks()
	 * @since 1.1
	 */
	@Override
	public Set<? extends ParallelTask> getTasks() {
		
		return new HashSet<>(this.tasks);
	}

	/**
	 * @see SequentialHandler#handle(ParallelTask)
	 * @since 1.0
	 */
	@Override
	public AbstractReturnType<?> handle(ParallelTask task) {
		
		tasks.add(task); // Add the task to the List.
		return null; // Return nothing.
	}

	/**
	 * Executes the stored tasks and returns their results.
	 * 
	 * @see SequentialHandler#postExecution()
	 * @since 1.0
	 */
	@Override
	public ThreadrResult postExecution() {
		
		ThreadrResult results = new ThreadrResult();
		
		for (ParallelTask task : tasks) {
			results.addResult(task, task.call());
		}
		
		return results; // Return the results.
	}
}
