package uk.ac.uea.threadr.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Runs all the unit tests for the Threadr framework.
 * 
 * @author Jordan Woerner
 */
@RunWith(Suite.class)
@SuiteClasses({
	SafetyTesterTests.class, 
	VMWrapperTests.class,
	ThreadrResultTests.class,
	ClassOperationsTests.class,
	ThreadrTests.class
})
public class AllTests { }