package uk.ac.uea.threadr.threadrtest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;
import java.util.logging.Level;

import uk.ac.uea.threadr.AbstractReturnType;
import uk.ac.uea.threadr.ParallelTask;
import uk.ac.uea.threadr.PostSequentialHandler;
import uk.ac.uea.threadr.Threadr;
import uk.ac.uea.threadr.ThreadrResult;
import uk.ac.uea.threadr.internal.MemoryMonitor;

public class StringTest implements ParallelTask {
	
	private ArrayList<String> strings;
	
	public StringTest() {
		
		strings = new ArrayList<>();
		Random random = new Random();
		
		for (int i = 0; i < ThreadrTest.SIZE; i++) {
			byte[] bytes = new byte[8];
			random.nextBytes(bytes);
			strings.add(new String(bytes));
		}
	}

	@Override
	public AbstractReturnType<?> call() {
		
		Collections.sort(strings, new StringComparator());
		
		ResultType returnType = new ResultType();
		
		returnType.setResult(strings);
		return returnType;
	}
	
	private class StringComparator implements Comparator<String> {

		@Override
		public int compare(String arg0, String arg1) {
		
			return arg0.compareTo(arg1);
		}
	}
	
	private class ResultType implements AbstractReturnType<ArrayList<?>> {
		
		private static final long serialVersionUID = 1L;
		private ArrayList<?> list;
		private String details;
		
		public ResultType() {
			
			details = MemoryMonitor.getInstance().toString();
		}
		
		@Override
		public void setResult(ArrayList<?> result) {
			list = result;
		}

		@Override
		public ArrayList<?> getResult() {
			return list;
		}
		
		@Override
		public String toString() {
			
			StringBuilder sb = new StringBuilder();
			/*
			for (int i = 0; i < 10; i++) {
				sb.append(list.get(i));
				if (i < 9) {
					sb.append(", ");
				}
			}
			*/
			sb.append(details);
			
			return sb.toString();
		}
	}
	
	public static void main(String...args) {
		
		Threadr threadr = new Threadr();
		threadr.enableLogging();
		threadr.setOutputLevel(Level.FINER);
		threadr.setSequentialHandler(new PostSequentialHandler());
		
		StringTest[] stringTests = new StringTest[5];
		Test2[] test2Tests = new Test2[5];
		
		for (int i = 0; i < 5; i++) {
			stringTests[i] = new StringTest();
			test2Tests[i] = new Test2();
		}
		
		threadr.addTasks(stringTests);
		threadr.addTasks(test2Tests);
		
		ThreadrResult results = threadr.execute();
		
		for (AbstractReturnType<?> result : results.getResults()) {
			if (result != null) {
				System.out.println(result.toString());
			} else {
				System.out.println("Null result");
			}
		}
	}
}
