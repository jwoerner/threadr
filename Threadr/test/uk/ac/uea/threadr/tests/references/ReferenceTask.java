package uk.ac.uea.threadr.tests.references;

import java.util.Arrays;
import java.util.Random;

import uk.ac.uea.threadr.AbstractReturnType;
import uk.ac.uea.threadr.ParallelTask;

/**
 * A reference class that implements the {@link ParallelTask} interface to be 
 * used in the unit tests for the Threadr framework.
 * 
 * @author Jordan Woerner
 */
public class ReferenceTask implements ParallelTask {
	
	/** Number of entries to store in the array. */
	public static final int SIZE = 50;
	/** Array of test data. */
	private int[] data;
	
	/**
	 * Create a new instance of the test data.
	 */
	public ReferenceTask() {
		
		data = new int[SIZE];
		
		Random ran = new Random();
		for (int i = 0; i < SIZE; i++) {
			data[i] = ran.nextInt();
		}
	}
	
	/**
	 * Get the test data stored in this ReferenceTask in the current state. 
	 * If you wish to use the current data as a reference, place this call in 
	 * a call to {@link Arrays#copyOf(int[], int)} to ensure you have a copy 
	 * and not a reference.
	 * 
	 * @return The data used in testing as an array of integers.
	 */
	public int[] getData() {
		
		return data;
	}
	
	/**
	 * The task for this test, a simple call to Arrays.sort() to sort an array 
	 * of integer values.
	 * 
	 * @return Returns an {@link ReferenceReturnType} that contains the array 
	 * held in this object. 
	 */
	@Override
	public final AbstractReturnType<?> call() {
		
		Arrays.sort(data);		
		ReferenceReturnType r = new ReferenceReturnType();
		r.setResult(data);
		
		return r; 
	}
}
