package uk.ac.uea.threadr.util;

import java.util.Random;
import java.util.logging.Logger;

import uk.ac.uea.threadr.Threadr;

/**
 * Generates Objects in various manners to create parameters for class 
 * instantiation.
 * 
 * @author Jordan Woerner
 * @version 1.0
 */
public class ParameterGen {

	/** Logger for this class. */
	private static Logger logger = Threadr.logger;
	
	private static final boolean[] bools = {true, false};
	private static final char[] chars = {'a', 'b', 'c', 'd', 'e', 'f'};
	private static final int[] ints = {-150, -100, -10, -5, -1, 0, 1, 5, 10, 100, 150};
	private static final double[] floats = {-100.0, -50.5, -5.25, -1.125, 0, 1.0, 5.5, 50.25, 100.125};
	private static final String[] strings = {"abc", "123", "rgb", "xyz", "a string", "Wh4cky_Str!g5"};
	
	/**
	 * Creates a new Object to be passed as a parameter to a method or  
	 * constructor.
	 * 
	 * @param paramType The {@link Class} type of the parameter required.
	 * @param level The depth of recursion for instantiation.
	 * @return An Object of the parameter type.
	 * @since 1.0
	 */
	static Object createParameter(Class<?> paramType, int level) {
		
		Random rand = new Random();
		
		/* Instantiate 'paramType' based on its type. */
		switch (paramType.getName()) {
		case "boolean":	// Value type bool.
			return  bools[rand.nextInt(bools.length)];
		case "byte":	// Value type byte.
			return  (byte)ints[rand.nextInt(ints.length)];
		case "char":	// Value type char.
			return  chars[rand.nextInt(chars.length)];
		case "short":	// Value type short.
			return  (short)ints[rand.nextInt(ints.length)];
		case "int":		// Value type int.
			return  ints[rand.nextInt(ints.length)];
		case "long":	// Value type long.
			return  (long)ints[rand.nextInt(ints.length)];
		case "float":	// Value type float.
			return  (float)floats[rand.nextInt(floats.length)];
		case "double":	// Value type double.
			return  ints[rand.nextInt(ints.length)];
		case "String":	// Reference type String.
			return  strings[rand.nextInt(strings.length)];
		default:		// Other Reference types.
			try {
				return ClassOperations.instantiateClass(paramType, level + 1);
			} catch (InstantiationException ex) {
				logger.warning(ex.getMessage());
				return null;
			}
		}
	}
	
	/**
	 * Creates a new array of Objects to be passed as a parameter to a method 
	 * or constructor.
	 *  
	 * @param paramType The {@link Class} type the array should contain.
	 * @param level The depth of recursion for instantiation.
	 * @param arraySize The size of the array to create.
	 * @return An array of Objects referencing the specified parameter type.
	 * @since 1.0
	 */
	static Object[] createParameter(Class<?> paramType, int level, int arraySize) {
		
		Object[] result = new Object[arraySize];
		
		for (int i = 0; i < arraySize; i++) {
			/* Generate parameters with the existing method. */
			result[i] = createParameter(paramType.getComponentType(), level);
			logger.finest(String.format("\t\t%s", result[i]));
		}
		
		return result;
	}
}
