package uk.ac.uea.threadr.internal.logging;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handles logging of events for the Threadr application. Specifies a text 
 * and HTML based log output. Logging level can be adjusted as needed at 
 * runtime.
 * 
 * @author Jordan Woerner
 * @version 1.1
 */
public class EventLogger {
	
	/** Store the configured instance of this class here. */
	private static EventLogger instance = null;
	
	/** Handler for text output. */
	private static Handler textOut;
	/** Hander for HTML output.  */
	private static Handler htmlOut;
	
	/**
	 * Configures the EventLogger instance for this JVM.
	 * 
	 * @throws IOException Thrown if the log files cannot be created for 
	 * any reason.
	 * @since 1.1
	 */
	private EventLogger() throws IOException {
		
		Logger logger = Logger.getLogger("uk.ac.uea.threadr");
		String fileDate = new SimpleDateFormat("yy_MM_dd-HH_mm").
				format(Calendar.getInstance().getTime());
		
		logger.setUseParentHandlers(false); // Don't use parent handlers.
		logger.setLevel(Level.INFO); // We want to see INFO or higher by default.
		
		String fileSep = System.getProperty("file.separator");
		String fileDir = String.format(".%slogs%s", fileSep, fileSep);
		
		File dir = new File(fileDir);
		dir.mkdirs();
		
		try {
			/* Create and join the text output to this Logger. */
			textOut = new FileHandler(String.format("%s%s.log", fileDir, fileDate));
			/* Create and join the HTML output to this Logger. */
			htmlOut = new FileHandler(String.format("%s%slog.html", fileDir, fileDate));
		} catch (IOException e) {
			throw new IOException("Could not create log files!", e);
		}
		textOut.setFormatter(new TextFormatter());
		logger.addHandler(textOut);
		
		htmlOut.setFormatter(new HTMLFormatter());
		logger.addHandler(htmlOut);
	}
	
	/**
	 * Sets up the EventLogger so that it can start logging.
	 * 
	 * @throws IOException Thrown if any of the output files fail to create.
	 * @since 1.0
	 */
	public static final void setup() throws IOException {

		/* Non-returning singleton pattern. */
		if (instance == null) {
			instance = new EventLogger();
		}
	}
	
	/**
	 * Changes the Logging level this EventLogger will log. Logging levels from
	 *  Java Logging API.
	 *  
	 * @see java.util.logging.Level
	 * @param level The logging level to use for this eventLogger.
	 * @since 1.0
	 */
	public static final void setLevel(Level level) {
	
		Logger logger = Logger.getLogger("uk.ac.uea.threadr");
		logger.setLevel(level);
	}
}
