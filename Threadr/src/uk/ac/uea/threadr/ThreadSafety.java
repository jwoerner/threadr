package uk.ac.uea.threadr;


/**
 * The results of a thread safety test execution. Allows a class to be treated 
 * as thread safe, virtual machine safe or completely unsafe for parallel 
 * execution.
 * 
 * @author Jordan Woerner
 * @version 1.1
 */
public enum ThreadSafety {
	
	/** 
	 * The class is only safe to be executed sequentially.
	 * @since 1.0
	 */
	SEQUENTIAL(0),
	/** 
	 * The class is safe to be executed in a thread.
	 * @since 1.0 
	 */
	THREAD(10),
	/** 
	 * The class is safe enough to be executed in a new JVM instance.
	 * @since 1.0
	 */
	VM(5);
	
	private int value;
	
	private ThreadSafety(int val) {
		value = val;
	}
	
	/**
	 * Gets the numeric value of this enum.
	 * @return The value of this enum as an integer.
	 * @since 1.1
	 */
	public int val() {
		
		return value;
	}
	
	/**
	 * Prints the textual representation of this enumeration.
	 * @return The textual representation of this enum as a String.
	 * @since 1.1
	 */
	@Override
	public String toString() {
		
		switch (value) {
		case 0:
			return "Sequential";
		case 5:
			return "Virtual Machine";
		case 10:
			return "Thread";
		default:
			return "ERROR";
		}
	}
}
