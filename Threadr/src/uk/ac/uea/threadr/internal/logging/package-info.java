/**
 * Handles logging of errors and output of the logs.
 * 
 * @author Jordan Woerner
 */
package uk.ac.uea.threadr.internal.logging;