package uk.ac.uea.threadr.internal.vmhandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import uk.ac.uea.threadr.AbstractReturnType;
import uk.ac.uea.threadr.ParallelTask;
import uk.ac.uea.threadr.internal.MemoryMonitor;

/**
 * Executes a provided {@link ParallelTask} and stores the result to be 
 * collected by the {@link VMWrapper} class after execution has ended.
 * 
 * @author Jordan Woerner
 * @version 1.0
 */
class VMInstance {

	/** The Class of the task to execute so it can be dynamically loaded. */
	private String taskHash;
	/** The task to execute. */
	private ParallelTask task;
	/** The result of execution to be stored. */
	private AbstractReturnType<?> result;
	/** Monitors the memory usage of this JVM. */
	private MemoryMonitor monitor;
	/** The thread that the MemoryMonitor will run in. */
	private Thread monitorThread;
	
	/** A singleton instance of VMInstance to prevent duplicate executions. */
	private static VMInstance instance;
	
	/**
	 * Executes the specified class in a new JVM.
	 * 
	 * @param args A list of command line arguments for this process.
	 */
	public static void main(String[] args) {
		
		String hash = null;
		
		try {
			hash = args[0];
		} catch(IndexOutOfBoundsException ex) {
			System.err.println("No arguments passed");
			System.exit(1);
		}
		if (hash != null) {
			VMInstance instance = VMInstance.getInstance(hash);
			instance.doTask();
			System.exit(0); // Exit with zero explicitly.
		} else {
			System.err.println("Invalid task hash provided!");
			System.exit(2); // Class null error.
		}
	}
	
	/**
	 * Set up this wrapper for the specified class so it can be executed later.
	 * 
	 * @param clazz The Class of the task to execute.
	 * @since 1.0
	 */
	private VMInstance(String hash) {
		
		this.taskHash = hash;
		
		this.monitor = MemoryMonitor.getInstance();
		this.monitorThread = new Thread(monitor);
		monitorThread.start();
	}
	
	/**
	 * Gets an instance of VMInstance. If one does not exist it will be 
	 * created then returned. 
	 * 
	 * @param clazz The Class of the {@link ParallelTask} to execute.
	 * @return An existing VMInstance if one exists, otherwise a new instance 
	 * will be created.
	 */
	static VMInstance getInstance(String hash) {
		
		if (instance == null) {
			instance = new VMInstance(hash);
		}
		
		return instance;
	}
	
	/**
	 * Grabs the data this class needs to execute.
	 * 
	 * @throws IOException Thrown if the data cannot be loaded.
	 * @throws ClassNotFoundException Thrown if the class required for 
	 * de-serialisation cannot be found.
	 * @since 1.0
	 */
	private void retrieveData() throws IOException, ClassNotFoundException {
		
		String fileSep = System.getProperty("file.separator");
		String tempDir = System.getProperty("java.io.tmpdir");
		String fileDir = String.format("%s%s", tempDir, 
				fileSep);
		String fileName = String.format("%s-in.tmp", taskHash);
		File resultFile = new File(fileDir, fileName);
		
		InputStream inStrm = null;
		ObjectInputStream objStrm = null;
		
		try {
			inStrm = new FileInputStream(resultFile);
			objStrm = new ObjectInputStream(inStrm);
			task = (ParallelTask)objStrm.readObject();
		} catch (ClassNotFoundException cfnEx) {
			throw new ClassNotFoundException(String.format("Could not find "
					+ "class '%s' whilst retriving data for task!", 
					taskHash));
		} catch (IOException ioEx) {
			throw new IOException(String.format("Could not access data for %s", 
					taskHash));
		} finally {
			/* Attempt to close the ObjectInputStream safely. */
			if (objStrm != null) {
				try {
					objStrm.close();
				} catch (IOException ioEx) {
					throw new IOException(String.format("Could not close the "
							+ "ObjectInputStream for task %s\nReason: %s", 
							taskHash, ioEx.getMessage()));
				} finally {
					/* Ensure it is always null in the end. */
					objStrm = null;
				}
			}
			/* Try to close the InputStream used safely. */
			if (inStrm != null) {
				try {
					inStrm.close();
				} catch (IOException ioEx) {
					throw new IOException(String.format("Could not close the "
							+ "InputStream for task %s\nReason: %s", 
							taskHash, ioEx.getMessage()));
				} finally {
					inStrm = null;
				}
			}
		}
	}
	
	/**
	 * Writes the result held by this JVM instance to a file to be 
	 * collected by the {@link VMWrapper} instance that created this JVM.
	 * 
	 * @throws IOException Thrown if the result cannot be written to file.
	 * @since 1.0
	 */
	private void storeResult() throws IOException {
		
		String fileSep = System.getProperty("file.separator");
		String tempDir = System.getProperty("java.io.tmpdir");
		String fileDir = String.format("%s%s", tempDir, 
				fileSep);
		String fileName = String.format("%s-out.tmp", taskHash);
		File resultFile = new File(fileDir, fileName);
		
		OutputStream oStream = null;
		ObjectOutputStream objStrm = null;
		
		try {
			oStream = new FileOutputStream(resultFile);
			objStrm = new ObjectOutputStream(oStream);
			
			objStrm.writeObject(result);
		} catch (IOException ioEx) {
			throw new IOException(String.format("Could not store result %s", 
					resultFile.getName()));
		} finally {
			/* Attempt to close the ObjectOutputStream if it was created. */
			if (objStrm != null) {
				try {
					objStrm.close();
				} catch (IOException ioEx) {
					throw new IOException(String.format("Could not close the "
							+ "ObjectOutputStream for task %s\nReason: %s", 
							taskHash, ioEx.getMessage()));
				}
			}
			/* Attempt to close the OutputStream used if it was created. */
			if (oStream != null) {
				try {
					oStream.close();
				} catch (IOException ioEx) {
					throw new IOException(String.format("Could not close the "
							+ "ObjectOutputStream for task %s\nReason: %s", 
							taskHash, ioEx.getMessage()));
				}
			}
		}
	}
	
	/**
	 * Gathers the data that this task needs to execute, executes the task, 
	 * then writes the data back to a file to be gathered by the 
	 * {@link VMWrapper} that created this JVM instance.   
	 * 
	 * @since 1.0
	 */
	private void doTask() {
		
		try {
			retrieveData();
			result = task.call();
			monitorThread.join();
			storeResult();
		} catch (Exception ex) {
			System.err.println("VM ERROR: " + ex.getMessage());
		}
	}
}