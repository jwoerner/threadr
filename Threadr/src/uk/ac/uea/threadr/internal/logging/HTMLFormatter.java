package uk.ac.uea.threadr.internal.logging;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Creates HTML log output for the Threadr EventLogger.
 * @author Jordan Woerner
 * @version 1.0
 */
class HTMLFormatter extends Formatter {

	/**
	 * Formats the LogRecord passed in to HTML.
	 */
	@Override
	public String format(LogRecord record) {
		 
		StringBuilder htmlBody = new StringBuilder();
		
		/* Create a new table row for the record. */
		htmlBody.append("\t\t\t\t\t<tr class=\"_" + record.getLevel().intValue() + "\">\n");
		/* Add the record level to the table. */
		htmlBody.append("\t\t\t\t\t\t<td>");
		htmlBody.append(record.getLevel());
		htmlBody.append("</td>\n");
		/* Add the record date to the table. */
		htmlBody.append("\t\t\t\t\t\t<td>");
		htmlBody.append(getDate(record.getMillis()));
		htmlBody.append("</td>\n");
		/* Add the record message to the table. */
		htmlBody.append("\t\t\t\t\t\t<td>");
		htmlBody.append(formatMessage(record));
		htmlBody.append("</td>\n");
		
		htmlBody.append("\t\t\t\t\t</tr>\n"); // End the table row.
		
		return htmlBody.toString();
	}
	
	/**
	 * Creates a HTML5 compliant HTML file header.
	 * @return A String containing the header.
	 */
	@Override
	public String getHead(Handler handler) {
		
		StringBuilder htmlHead = new StringBuilder();
		htmlHead.append("<!DOCTYPE html>\n");
		htmlHead.append("<html>\n");
		htmlHead.append("\t<head>\n");
		htmlHead.append("\t\t<meta charset=\"utf-8\" />\n");
		htmlHead.append("\t\t<title>Threadr Log</title>\n");
		htmlHead.append("\t\t<link rel=\"stylesheet\" href=\"css\\normalize.css\" />\n");
		htmlHead.append("\t\t<link rel=\"stylesheet\" href=\"css\\style.css\" />\n");
		htmlHead.append("\t\t<script src=\"js\\jquery.js\"></script>\n");
		htmlHead.append("\t\t<script src=\"js\\events.js\"></script>\n");
		htmlHead.append("\t</head>\n");
		/* Create the header. */
		htmlHead.append("\t<body>\n");
		htmlHead.append("\t\t<div id=\"header\">\n");
		htmlHead.append("\t\t\t<h1>Threadr Log</h1>\n");
		htmlHead.append("\t\t\t<h2>");
		htmlHead.append(getDate(Calendar.getInstance().getTimeInMillis()));
		htmlHead.append("</h2>\n");
		htmlHead.append("\t\t</div>\n");
		/* Create the log summary table. */
		htmlHead.append("\t\t<h3>Log Summary</h3>\n");
		htmlHead.append("\t\t<div id=\"logSummary\">\n");
		htmlHead.append("\t\t\t<table>\n");
		htmlHead.append("\t\t\t\t<tbody>\n");
		htmlHead.append("\t\t\t\t\t<tr class=\"_1000\">\n");
		htmlHead.append("\t\t\t\t\t\t<td>SEVERE</td>\n");
		htmlHead.append("\t\t\t\t\t\t<td id=\"severeCount\">0</td>\n");
		htmlHead.append("\t\t\t\t\t</tr>\n");
		htmlHead.append("\t\t\t\t\t<tr class=\"_900\">\n");
		htmlHead.append("\t\t\t\t\t\t<td>WARNING</td>\n");
		htmlHead.append("\t\t\t\t\t\t<td id=\"warningCount\">0</td>\n");
		htmlHead.append("\t\t\t\t\t</tr>\n");
		htmlHead.append("\t\t\t\t\t<tr class=\"_800\">\n");
		htmlHead.append("\t\t\t\t\t\t<td>INFO</td>\n");
		htmlHead.append("\t\t\t\t\t\t<td id=\"infoCount\">0</td>\n");
		htmlHead.append("\t\t\t\t\t</tr>\n");
		htmlHead.append("\t\t\t\t\t<tr class=\"_700\">\n");
		htmlHead.append("\t\t\t\t\t\t<td>CONFIG</td>\n");
		htmlHead.append("\t\t\t\t\t\t<td id=\"configCount\">0</td>\n");
		htmlHead.append("\t\t\t\t\t</tr>\n");
		htmlHead.append("\t\t\t\t\t<tr class=\"_500\">\n");
		htmlHead.append("\t\t\t\t\t\t<td>FINE</td>\n");
		htmlHead.append("\t\t\t\t\t\t<td id=\"fineCount\">0</td>\n");
		htmlHead.append("\t\t\t\t\t</tr>\n");
		htmlHead.append("\t\t\t\t\t<tr class=\"_400\">\n");
		htmlHead.append("\t\t\t\t\t\t<td>FINER</td>\n");
		htmlHead.append("\t\t\t\t\t\t<td id=\"finerCount\">0</td>\n");
		htmlHead.append("\t\t\t\t\t</tr>\n");
		htmlHead.append("\t\t\t\t\t<tr class=\"_300\">\n");
		htmlHead.append("\t\t\t\t\t\t<td>FINEST</td>\n");
		htmlHead.append("\t\t\t\t\t\t<td id=\"finestCount\">0</td>\n");
		htmlHead.append("\t\t\t\t\t</tr>\n");
		htmlHead.append("\t\t\t\t</tbody>\n");
		htmlHead.append("\t\t\t</table>\n");
		htmlHead.append("\t\t</div>\n");
		/* Create the main log details table. */
		htmlHead.append("\t\t<h3>Log Details</h3>\n");
		htmlHead.append("\t\t<div id=\"log\">\n");
		htmlHead.append("\t\t\t<table id=\"logTable\">\n");
		htmlHead.append("\t\t\t\t<thead>\n");
		htmlHead.append("\t\t\t\t\t<tr>\n");
		htmlHead.append("\t\t\t\t\t\t<th class=\"levelHeader\">Severity<img src=\"img/bullet_arrow.png\"></th>\n");
		htmlHead.append("\t\t\t\t\t\t<th class=\"timeHeader\">Time<img src=\"img/bullet_arrow.png\"></th>\n");
		htmlHead.append("\t\t\t\t\t\t<th>Message</th>\n");
		htmlHead.append("\t\t\t\t\t</tr>\n");
		htmlHead.append("\t\t\t\t</thead>\n");
		htmlHead.append("\t\t\t\t<tfoot>\n");
		htmlHead.append("\t\t\t\t\t<tr>\n");
		htmlHead.append("\t\t\t\t\t\t<td class=\"levelHeader\">Severity<img src=\"img/bullet_arrow.png\"></td>\n");
		htmlHead.append("\t\t\t\t\t\t<td class=\"timeHeader\">Time<img src=\"img/bullet_arrow.png\"></td>\n");
		htmlHead.append("\t\t\t\t\t\t<td>Message</td>\n");
		htmlHead.append("\t\t\t\t\t</tr>\n");
		htmlHead.append("\t\t\t\t</tfoot>\n");
		htmlHead.append("\t\t\t\t<tbody>\n");
		
		return htmlHead.toString();
	}
	
	/**
	 * Creates a HTML file footer.
	 * @return A String containing the footer.
	 */
	@Override
	public String getTail(Handler handler) {
	
		StringBuilder htmlTail = new StringBuilder();
		htmlTail.append("\t\t\t\t</tbody>\n");
		htmlTail.append("\t\t\t</table>\n");
		htmlTail.append("\t\t</div>\n");
		htmlTail.append("\t</body>\n");
		htmlTail.append("</html>");
		
		return htmlTail.toString();
	}
	
	/**
	 * Formats the provided time into a legible date.
	 * @param time The date to format in milliseconds.
	 * @return The date as a String in format dd-mm-yy hh:mm.
	 */
	public String getDate(long time) {
		
		String date = new SimpleDateFormat("dd/MM/yy hh:mm:ss").
				format(new Date(time));
		
		return date;
	}
}
