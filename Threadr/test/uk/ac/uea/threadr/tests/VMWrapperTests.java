package uk.ac.uea.threadr.tests;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;

import org.junit.Test;

import uk.ac.uea.threadr.AbstractReturnType;
import uk.ac.uea.threadr.ParallelTask;
import uk.ac.uea.threadr.internal.vmhandler.VMWrapper;
import uk.ac.uea.threadr.tests.references.ReferenceTask;
import uk.ac.uea.threadr.tests.references.ReferenceVMTask;

/**
 * Tests the VM execution functionality to ensure the result from a VM matches 
 * a reference result.
 * 
 * @author Jordan Woerner
 */
public final class VMWrapperTests {	

	/**
	 * Test the correctness of output from the VMWrapper class. This test 
	 * ensures that the instantiated virtual machine is actually processing 
	 * the tasks passed to it correctly. 
	 */
	@Test
	public final void testCorrectness() {
	
		ReferenceVMTask task = new ReferenceVMTask();
		int[] reference = Arrays.copyOf(task.getData(), ReferenceTask.SIZE);
		
		Arrays.sort(reference);
		
		VMWrapper newVM = new VMWrapper(task);
		try {
			AbstractReturnType<?> returned = newVM.call(); // run the task.
			assertArrayEquals("Result different from reference", reference, 
					(int[])returned.getResult());
		} catch (Exception e) {
			fail("Error executing new Virtual Machine");
		}
	}
	
	/**
	 * Test the {@link VMWrapper#equals(Object)} method is working correctly.
	 */
	@Test
	public final void testEquality() {
				
		ParallelTask task1 = new ReferenceTask();
		
		VMWrapper wrapper1Task1 = new VMWrapper(task1);
		VMWrapper wrapper2Task1 = new VMWrapper(task1);
		VMWrapper wrapper1Task2 = new VMWrapper(new ParallelTask() {			
			@Override
			public AbstractReturnType<?> call() {
				return null; // Don't do anything.
			}
		});
		
		/* Test various equality comparisons to ensure equals() works. */
		assertTrue("Same wrapper objects different.", 
				wrapper1Task1.equals(wrapper1Task1));
		assertTrue("Same task wrappers different.", 
				wrapper1Task1.equals(wrapper2Task1));
		assertTrue("Same task wrappers different reveresed.", 
				wrapper2Task1.equals(wrapper1Task1));
		assertFalse("Different task wrappers equal.", 
				wrapper1Task2.equals(wrapper1Task1));
		assertFalse("Different task wrappers equal reveresed.", 
				wrapper1Task1.equals(wrapper1Task2));
	}
}
