/*jslint browser: true*/
/*global $, window, document, jQuery, console*/

function countLogs(jQuery) {
    
    'use strict';
    /* Get the number of each log record type. */
    var severeCount = $('#logTable > tbody > tr._1000').length,
        warningCount = $('#logTable > tbody > tr._900').length,
        infoCount = $('#logTable > tbody > tr._800').length,
        configCount = $('#logTable > tbody > tr._700').length,
        fineCount = $('#logTable > tbody > tr._500').length,
        finerCount = $('#logTable > tbody > tr._400').length,
        finestCount = $('#logTable > tbody > tr._300').length;
    
    console.log("Severe: " + severeCount);
    console.log("Warning: " + warningCount);
    console.log("Info: " + infoCount);
    console.log("Config: " + configCount);
    console.log("Fine: " + fineCount);
    console.log("Finer: " + finerCount);
    console.log("Finest: " + finestCount);
    
    /* Change the values shown in the log summary. */
    $('#severeCount').text(severeCount);
    $('#warningCount').text(warningCount);
    $('#infoCount').text(infoCount);
    $('#configCount').text(configCount);
    $('#fineCount').text(fineCount);
    $('#finerCount').text(finerCount);
    $('#finestCount').text(finestCount);
}

function sortTable(jQuery) {
    
    'use strict';
    var table = $('#logTable');

    $('.levelHeader, .timeHeader')
        .each(function () {
            var th = $(this), thIndex = th.index(), inverse = false;
            th.click(function () {
                table.find('td').filter(function () {
                    return $(this).index() === thIndex;
                }).sortElements(function (a, b) {
                    if ($.text([a]) === $.text([b])) {
                        return 0;
                    }
                   
                    if (inverse) {
                        if ($.text([a]) > $.text([b])) {
                            return 1;
                        } else {
                            return -1;
                        }
                    } else {
                        if ($.text([a]) > $.text([b])) {
                            return -1;
                        } else {
                            return 1;
                        }
                    }
                }, function () {
                    return this.parentNode;
                });
                
                inverse = !inverse;
            });
        });
}

window.onload = function () {
    
    'use strict';
    countLogs();
    sortTable();
};