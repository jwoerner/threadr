package uk.ac.uea.threadr.tests.references;

import uk.ac.uea.threadr.ThreadSafe;

/**
 * A reference class to test the {@link ClassOperation} methods for accessing
 * the details of a class.
 * 
 * @author Jordan Woerner
 */
public final class ReferenceClass {

	private int val;
	@ThreadSafe
	public static String desc = "Test";
	protected volatile float protectedVolatile;

	public ReferenceClass() {
		val = 0;
	}

	public int getVal() {
		return val;
	}
	
	public float getFloatVal() {
		return protectedVolatile;
	}
	
	public static String getDesc() {
		return desc;
	}
}