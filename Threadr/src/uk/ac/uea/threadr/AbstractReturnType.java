package uk.ac.uea.threadr;

import java.io.Serializable;

/**
 * Provides a method to get data back from ParallelTasks.
 *  
 * @author Jordan Woerner
 * @version 1.0
 */
public interface AbstractReturnType<V> extends Serializable {

	/**
	 * Sets the result held in this AbstractReturnType.
	 * 
	 * @param result The result as an instance of the type parameter.
	 * @since 1.0
	 */
	public void setResult(V result);
	
	/**
	 * Gets the result held in this AbstractReturnType.
	 * 
	 * @return The result as an instance of the type parameter.
	 * @since 1.0
	 */
	public V getResult();
}
