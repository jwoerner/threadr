package uk.ac.uea.threadr;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>Apply this Annotation Type to any fields, members or methods that are  
 * known to be thread safe. This allows Threadr to avoid processing elements of
 *  classes that cannot cause thread safety violations.</p>
 * <p>This Annotation can only be applied to methods and fields.</p>
 * <p>This Annotation is visible at runtime, allowing for it to be used in 
 * thread safety tests used by the Threadr framework if needed.</p>
 * 
 * @author Jordan Woerner
 * @version 1.0
 */
@Target({
	ElementType.CONSTRUCTOR, 
	ElementType.METHOD, 
	ElementType.FIELD
	})
@Retention(RetentionPolicy.RUNTIME)
public @interface ThreadSafe {}
