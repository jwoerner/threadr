package uk.ac.uea.threadr.internal.logging;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Creates text file output for the Threadr EventLogger class.
 * @author Jordan Woerner
 * @version 1.0
 */
class TextFormatter extends Formatter {

	/**
	 * Outputs the provided LogRecord to a text file.
	 * @return The LogRecord in the output format.
	 */
	@Override
	public String format(LogRecord record) {

		StringBuilder textBody = new StringBuilder();
		
		textBody.append(getDate(record.getMillis()));
		textBody.append(" - ");
		textBody.append(record.getLevel());
		textBody.append(": ");
		textBody.append(formatMessage(record));
		textBody.append("\r\n");
		System.out.print(textBody);
		return textBody.toString();
	}
	
	/**
	 * Gets the header for text logs.
	 * @return The header for text logs as a String.
	 */
	@Override
	public String getHead(Handler handler) {
		
		StringBuilder textHead = new StringBuilder();
		
		textHead.append("Threadr Log - ");
		textHead.append(getDate(Calendar.getInstance().getTimeInMillis()));
		textHead.append("\r\n");
		
		return textHead.toString();
	}
	
	/**
	 * Gets the footer for text logs.
	 * @return The header for text logs as a String.
	 */
	@Override
	public String getTail(Handler handler) {
		
		StringBuilder textTail = new StringBuilder();
		
		textTail.append("Log End - ");
		textTail.append(getDate(Calendar.getInstance().getTimeInMillis()));
		
		return textTail.toString();
	}
	
	/**
	 * Formats the provided time into a legible date.
	 * @param time The date to format in milliseconds.
	 * @return The date as a String in format dd/mm/yy hh:mm.
	 */
	public String getDate(long time) {
		
		String date = new SimpleDateFormat("dd/MM/yy hh:mm:ss").
				format(new Date(time));
		
		return date;
	}
}