package uk.ac.uea.threadr.threadrtest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import uk.ac.uea.threadr.AbstractReturnType;
import uk.ac.uea.threadr.ParallelTask;
import uk.ac.uea.threadr.Threadr;
import uk.ac.uea.threadr.ThreadrResult;

public class ThreadrTest2 implements ParallelTask, Serializable {

	private static final long serialVersionUID = -8522415360051890792L;
	static int SIZE = 5000;
	private int[] data = new int[SIZE];
	
	public static void main(String[] args) {
		
		List<ParallelTask> tasks = new ArrayList<>();
		
		tasks.add(new ThreadrTest2());
		tasks.add(new ThreadrTest2());
		tasks.add(new ThreadrTest2());
		tasks.add(new FirstParallelSafe());
		tasks.add(new FirstParallelSafe());
		tasks.add(new FirstParallelSafe());
		
		Threadr threadr = new Threadr(tasks);
		threadr.enableLogging();
		
		ThreadrResult results = threadr.execute();
		
		for (Object result : results.getResults()) {
			try {
				SomeReturnType r = (SomeReturnType)result;
				if (r == null) {
					System.err.println("Result was null.");
					continue;
				}
				if (r.isSorted()) {
					System.out.print("Sorted - ");
				} else {
					System.err.print("Not Sorted - ");
				}
				System.out.println(r);
			} catch (ClassCastException ex) {
				System.err.println("Could not convert to SomeReturnType");
			}
		}
	}
	
	public ThreadrTest2() {
		
		Random random = new Random();
		for (int i = 0; i < SIZE; i++) {
			data[i] = random.nextInt(SIZE*4);
		}
	}
	
	@Override
	public AbstractReturnType<?> call() {

		boolean sorted = false;
		while(!sorted) {
			sorted = true;
			for (int i = 1; i < SIZE; i++) {
				if (data[i-1] > data[i]) {
					int tmp = data[i];
					data[i] = data[i - 1];
					data[i - 1] = tmp;
					sorted = false;
				}
			}
		}
		
		SomeReturnType result = new SomeReturnType();
		result.setResult(data);
		
		return result;
	}
	
	private static class FirstParallelSafe implements ParallelTask, Serializable {
		
		private static final long serialVersionUID = 6471655579396783245L;
		private int[] data = new int[SIZE];
		private static boolean sorted = false;
		
		public FirstParallelSafe() {
			
			Random random = new Random();
			for (int i = 0; i < SIZE; i++) {
				data[i] = random.nextInt(SIZE*4);
			}
		}
		
		@Override
		public AbstractReturnType<?> call() {

			while(!sorted) {
				sorted = true;
				for (int i = 1; i < SIZE; i++) {
					if (data[i-1] > data[i]) {
						int tmp = data[i];
						data[i] = data[i - 1];
						data[i - 1] = tmp;
						sorted = false;
					}
				}
			}
			
			SomeReturnType result = new SomeReturnType();
			result.setResult(data);
			
			return result;
		}
	}
	
	private static class SomeReturnType implements AbstractReturnType<int[]> {

		private static final long serialVersionUID = -5535932419636521189L;
		private int[] data;
		
		@Override
		public void setResult(int[] result) {
			data = result;
		}

		@Override
		public int[] getResult() {
			return data;
		}
		
		public boolean isSorted() {
			
			for (int i = 0; i < SIZE-1; i++) {
				if (data[i] > data[i+1]) {
					return false;
				}
			}
			
			return true;
		}
		
		@Override
		public String toString() {

			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < 20; i++) {
				sb.append(data[i]);
				if (i < 19) sb.append(", ");
			}
			
			return sb.toString();
		}
	}
}
