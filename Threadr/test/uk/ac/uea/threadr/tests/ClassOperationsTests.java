package uk.ac.uea.threadr.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import uk.ac.uea.threadr.ThreadSafe;
import uk.ac.uea.threadr.tests.references.ReferenceClass;
import uk.ac.uea.threadr.util.ClassOperations;
import uk.ac.uea.threadr.util.Operation;

/**
 * Tests the {@link ClassOperations} class to ensure that it can construct 
 * arbitrary classes, find classes in packages and gather the correct details  
 * about a specified class.
 * 
 * @author Jordan Woerner
 */
public final class ClassOperationsTests {

	/**
	 * Tests the class instantiation feature of the {@link ClassOperations} 
	 * class. Fails if any of the reference types are still null or the integer
	 *  type has not changed.
	 */
	@SuppressWarnings("unchecked") // This shouldn't be a problem.
	@Test
	public final void testInstantiateClass() {

		int testInt = Integer.MIN_VALUE;
		Integer testRefInt = null;
		String testString = null;
		ArrayList<Double> testList = null;
		
		try {
			testInt = (int)ClassOperations.instantiateClass(Integer.class);
			testRefInt = (Integer)ClassOperations.instantiateClass(Integer.class);
			testString = (String)ClassOperations.instantiateClass(String.class);
			testList = (ArrayList<Double>)ClassOperations.instantiateClass(ArrayList.class);
		} catch (InstantiationException e) {
			e.printStackTrace();
		}
		
		assertNotEquals("int was not created.", 0,  testInt);
		assertNotNull("Integer reference type not instantiated.", testRefInt);
		assertNotNull("String not instantiated.", testString);
		assertNotNull("ArrayList not instantiated.", testList);
	}

	/**
	 * Searches for the classes in this package. This test fails if the found 
	 * classes are different from the reference classes.
	 */
	@Test
	public final void testFindClassesInPackage() {
		
		List<Class<?>> reference = new ArrayList<>();
		reference.add(AllTests.class);
		reference.add(ClassOperationsTests.class);
		reference.add(ClassOperationsTests.class);
		reference.add(SafetyTesterTests.class);
		reference.add(ThreadrResultTests.class);
		reference.add(VMWrapperTests.class);
		
		List<Class<?>> found = ClassOperations.findClassesInPackage(
				"uk.ac.uea.threadr.tests", false);
		
		assertTrue(found.containsAll(reference));
	}

	/**
	 * Attempts to get the constructors for {@link ReferenceClass}. Fails if 
	 * anything but two constructors are found.
	 */
	@Test
	public final void testGetConstructors() {

		Operation[] constructors = ClassOperations
				.getConstructors(ReferenceClass.class);
		
		assertEquals("Incorrect number of constructors found.", 
				1, constructors.length);
	}

	/**
	 * Attempts to get the methods for {@link ReferenceClass}. Fails if 
	 * anything but one method is found.
	 */
	@Test
	public final void testGetMethods() {

		Operation[] methods = ClassOperations
				.getMethods(ReferenceClass.class);
		
		assertEquals("Incorrect number of methods found.", 3, 
				methods.length);
	}

	/**
	 * Attempts to get the fields for {@link ReferenceClass}. Fails if 
	 * anything but three fields are found.
	 */
	@Test
	public final void testGetFields() {
		
		Operation[] fields = ClassOperations
				.getFields(ReferenceClass.class);
		
		assertEquals("Incorrect number of fields found.",
				3, fields.length);
	}

	/**
	 * Checks for the {@link ThreadSafe} annotation in the 
	 * {@link ReferenceClass}. Fails if the annotation is not found at all or 
	 * if the annotation is found on the wrong {@link Operation}.
	 */
	@Test
	public final void testHasAnnotation() {

		Operation[] fields = ClassOperations
				.getFields(ReferenceClass.class);
		
		for (Operation op : fields) {
			if (op.getReturnType() == Integer.class) {
				assertTrue("Failed to find '@ThreadSafe' annotation." ,
						ClassOperations.hasAnnotation(op.getClass(), 
								ThreadSafe.class));
			} else {
				assertFalse("Found '@ThreadSafe' annotation on wrong field." ,
						ClassOperations.hasAnnotation(op.getClass(), 
								ThreadSafe.class));
			}
		}
	}
}
