package uk.ac.uea.threadr.internal.testing.tests;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Logger;

import uk.ac.uea.threadr.Threadr;

/**
 * Tests a class to see if it is safe to serialise or not.
 * 
 * @author Jordan Woerner
 * @version 1.0
 */
public class SerializableTest {

	/** The logger used by Threadr. */
	private static final Logger logger = Threadr.logger;

	/**
	 * Tests the provided object to see if it can be serialised or not.
	 * 
	 * @return Returns true if the object is safe to serialise, false if it 
	 * fails for any reason.
	 * @since 1.0
	 */
	public boolean test(final Object testItem) {
		
		Object origional = testItem;
		byte[] serialised = null;
		
		try {
			/* Attempt to serialise the object. */
			ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
			ObjectOutputStream objOut = new ObjectOutputStream(byteOut);
			objOut.writeObject(origional);
			serialised = byteOut.toByteArray();
		} catch (IOException e) {
			/* If serialisation fails for any reason. */
			logger.info(String.format("Failed to serialize class %s.", 
					origional.getClass().getName()));
			return false;
		}
		
		try {
			/* Try and de-serialise the object to see if that works. */
			ByteArrayInputStream byteIn = new ByteArrayInputStream(serialised);
			ObjectInputStream objIn = new ObjectInputStream(byteIn);
			objIn.readObject();
		} catch (IOException ioEx) {
			/* If de-serialising fails due to IO errors, log this. */
			logger.info(String.format("Could not deserialize class %s.",
					origional.getClass().getName()));
			return false;
		} catch (ClassNotFoundException classEx) {
			/* If the Class cannot be found for any reason, severe error. */
			logger.severe(String.format("Failed to find class %s!", 
					origional.getClass().getName()));
			return false;
		}
		
		return true; // If nothing fails, return true.
	}

}
