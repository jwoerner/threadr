/**
 * Contains the JUint test cases and test suites.
 * 
 * @author Jordan Woerner
 */
package uk.ac.uea.threadr.tests;