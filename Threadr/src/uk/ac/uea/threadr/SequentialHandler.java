package uk.ac.uea.threadr;

import java.util.Set;

/**
 * Provides a method to handle tasks that are not safe to run concurrently.
 * To use effectively it is recommended to implement some form of internal 
 * storage of {@link ParallelTask}s such as a List.
 * 
 * @author Jordan Woerner
 * @version 1.1
 */
public interface SequentialHandler {
	
	/**
	 * Gets the tasks held inside this SequentialHandler.
	 * 
	 * @return The {@link ParallelTask}s this SequentialHandler has stored as 
	 * a {@link Set}.
	 */
	public Set<? extends ParallelTask> getTasks();
	
	/**
	 * Called when a task is being handled by Threadr (in the case of 
	 * concurrent-safe tasks, being wrapped in threads or sent to a new JVM 
	 * instance).
	 * 
	 * @return The {@link AbstractReturnType} result of the task handled. If 
	 * the implementation of SequentialHandler does not need to return anything
	 *  here, return null.
	 * @since 1.0
	 */
	public AbstractReturnType<?> handle(ParallelTask task);
	
	/**
	 * Called after Threadr is finished creating new threads and JVM instances.
	 * 
	 * @return A ThreadrResult object mapping tasks to the results.. If nothing 
	 *  is done in this method, or the results are not needed, return null.
	 * @since 1.0
	 */
	public ThreadrResult postExecution();
}
