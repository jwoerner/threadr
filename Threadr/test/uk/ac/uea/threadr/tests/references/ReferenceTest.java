package uk.ac.uea.threadr.tests.references;

import uk.ac.uea.threadr.ThreadSafety;
import uk.ac.uea.threadr.ThreadTest;

/**
 * A reference class that implements the {@link ThreadTest} interface to be 
 * used in the unit tests for the Threadr framework.
 * 
 * @author Jordan Woerner
 */
public class ReferenceTest implements ThreadTest {

	@Override
	public ThreadSafety test(Class<?> clazz) { return ThreadSafety.SEQUENTIAL; }
}
