/**
 * Classes to handle the creation and interaction with a new instance of the 
 * Java Virtual Machine including the cloning of classes into this new JVM 
 * instance and the returning of results from the JVM instances.
 * 
 * @author Jordan Woerner
 */
package uk.ac.uea.threadr.internal.vmhandler;