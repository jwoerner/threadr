package uk.ac.uea.threadr.tests.references;

/**
 * An extended implementation of {@link ReferenceTask} that includes a thread 
 * safety violation and isn't serialisable making it unsafe for virtual 
 * machines.
 * 
 * @author Jordan Woerner
 */
public class ReferenceUnsafeTask extends ReferenceTask {

	/** This causes a thread safety violation. */
	public static String details = "This is unsafe";
}
