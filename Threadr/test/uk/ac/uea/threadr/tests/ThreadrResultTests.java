package uk.ac.uea.threadr.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import uk.ac.uea.threadr.AbstractReturnType;
import uk.ac.uea.threadr.ParallelTask;
import uk.ac.uea.threadr.ThreadrResult;
import uk.ac.uea.threadr.tests.references.ReferenceReturnType;
import uk.ac.uea.threadr.tests.references.ReferenceTask;

/**
 * Tests the {@link ThreadrResult} class to ensure it stores tasks and results 
 * as expected.
 * 
 * @author Jordan Woerner
 */
public final class ThreadrResultTests {

	/** The consistent ThreadrResult to work with. */
	private ThreadrResult result;
	/** Some reference tasks to use in testing. */
	private ParallelTask[] tasks;
	
	/**
	 * Set up the reference {@link ThreadrResult} instance and create the 
	 * reference tasks.
	 * 
	 * @throws Exception Not thrown in this method.
	 */
	@Before
	public final void setUp() throws Exception {
		
		result = new ThreadrResult();
		tasks = new ReferenceTask[2];
		tasks[0] = new ReferenceTask();
		tasks[1] = new ReferenceTask();
	}

	/**
	 * Ensures that tasks can be added to a ThreadrResult properly. This test 
	 * only fails if the task cannot be added for any reason.
	 */
	@Test
	public final void testAddTask() {

		assertTrue("Failed to add task", result.addTask(tasks[0]));
		assertTrue("Task not stored in ThreadrResult", 
				result.getTasks().contains(tasks[0]));
	}

	/**
	 * Attempts to add results to the tasks stored in the ThreadrResult 
	 * instance. This test will fail if the first call returns non-null or the 
	 * second call returns null.
	 */
	@Test
	public final void testAddResult() {
		
		assertNull("Failed to add result to stored task.", 
				result.addResult(tasks[0], new ReferenceReturnType()));
		assertNotNull("Existing result not returned.", 
				result.addResult(tasks[0], new ReferenceReturnType()));
	}

	/**
	 * Attempts to remove a result from the ThreadrResult. Fails if the stored 
	 * result for a stored task is not returned, or if a result is returned for
	 *  an unstored task.
	 */
	@Test
	public final void testRemoveResult() {

		result.addTask(tasks[0]);
		result.addResult(tasks[0], new ReferenceReturnType());
		assertNotNull("Result not returned.", result.removeResult(tasks[0]));
		assertNull("Unstored task returned result.", 
				result.removeResult(tasks[1]));
	}

	/**
	 * Tests the {@link ThreadrResult#combine(ThreadrResult)} method. Fails if 
	 * the conflicting result is not overwritten.
	 */
	@Test
	public final void testCombineThreadrResult() {
		
		ThreadrResult other = new ThreadrResult();
		ReferenceReturnType ref = new ReferenceReturnType();
		ReferenceReturnType conflict = new ReferenceReturnType();		
		
		result.addTask(tasks[0]);
		result.addResult(tasks[0], ref);
		
		other.addTask(tasks[0]);
		other.addTask(tasks[1]);
		other.addResult(tasks[0], conflict);
		other.addResult(tasks[1], new ReferenceReturnType());
		
		result.combine(other);
		assertNotNull("Result became null", result);
		assertEquals("Incorrect number of tasks stored.", 2, result.size());
		
		AbstractReturnType<?> conflictingResult = result.getResult(tasks[0]);
		
		assertNotNull("Conflicting result was null.", conflictingResult);
		assertNotEquals("Result was not overwritten.", ref, conflictingResult);
		assertEquals("Result was not overwritten.", conflict, conflictingResult);
	}

	/**
	 * Tests the {@link ThreadrResult#combine(ThreadrResult, boolean)} method. 
	 * Fails if the conflicting result is overwritten.
	 */
	@Test
	public final void testCombineThreadrResultNoOverwrite() {
		
		ThreadrResult other = new ThreadrResult();
		ReferenceReturnType ref = new ReferenceReturnType();
		ReferenceReturnType conflict = new ReferenceReturnType();		
		
		result.addTask(tasks[0]);
		result.addResult(tasks[0], ref);
		
		other.addTask(tasks[0]);
		other.addTask(tasks[1]);
		other.addResult(tasks[0], conflict);
		other.addResult(tasks[1], new ReferenceReturnType());
		
		result.combine(other, false);
		assertNotNull("Result became null", result);
		assertEquals("Incorrect number of tasks stored.", 2, result.size());
		
		AbstractReturnType<?> conflictingResult = result.getResult(tasks[0]);
		
		assertNotNull("Conflicting result was null.", conflictingResult);
		assertEquals("Result was overwritten.", ref, conflictingResult);
		assertNotEquals("Result was overwritten.", 
				conflict, conflictingResult);
	}

	/**
	 * Tests to ensure that {@link ThreadrResult#size()} returns the correct 
	 * values.
	 */
	@Test
	public final void testSize() {
		
		assertEquals("size() returned > 0.", 0, result.size());
		result.addTask(tasks[0]);
		assertEquals(1, result.size());
	}

}
