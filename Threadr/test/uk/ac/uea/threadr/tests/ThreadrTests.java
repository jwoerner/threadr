package uk.ac.uea.threadr.tests;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import uk.ac.uea.threadr.ParallelTask;
import uk.ac.uea.threadr.PostSequentialHandler;
import uk.ac.uea.threadr.ThreadSafety;
import uk.ac.uea.threadr.ThreadTest;
import uk.ac.uea.threadr.Threadr;
import uk.ac.uea.threadr.ThreadrResult;
import uk.ac.uea.threadr.internal.MemoryMonitor;
import uk.ac.uea.threadr.tests.references.ReferenceTask;
import uk.ac.uea.threadr.tests.references.ReferenceTest;
import uk.ac.uea.threadr.tests.references.ReferenceUnsafeTask;
import uk.ac.uea.threadr.tests.references.ReferenceVMTask;

/**
 * Tests the functionality of the {@link Threadr} class to make sure the API 
 * works as expected.
 * 
 * @author Jordan Woerner
 */
public class ThreadrTests {

	/** The number of reference items to create for testing. */
	private static final int REFERENCE_COUNT = 5;
	/** The Threadr instance to work with. */
	private Threadr instance = null;
	
	/**
	 * Test the default constructor ({@link Threadr#Threadr()}. It should 
	 * create an instance of the framework with no tasks or tests provided.
	 */
	@Test
	public final void testDefaultConstructor() {
		
		instance = new Threadr();
		
		assertNotNull("Failed to instantiate Threadr.", instance);
		assertEquals("Default constructor provided tasks.", 0, 
				instance.getTasks().size());
		assertEquals("Default constructor provided tests.", 0, 
				instance.getTests().size());
	}

	/**
	 * Test one of the parametrised constructors 
	 * ({@link Threadr#Threadr(ParallelTask[]). The constructor accepts an 
	 * array of {@link ParallelTask} objects. The test fails if Threadr 
	 * contains an incorrect number of tasks once instantiated.
	 */
	@Test
	public final void testTaskArrayConstructor() {
		
		ParallelTask[] tasks = new ParallelTask[REFERENCE_COUNT];
		
		for (int i = 0; i < REFERENCE_COUNT; i++) {
			tasks[i] = new ReferenceTask();
		}
		
		instance = new Threadr(tasks);
		
		assertNotNull("Failed to instantiate Threadr.", instance);
		assertEquals("Incorrect number of tasks found.", REFERENCE_COUNT, 
				instance.getTasks().size());
		assertEquals("Array constructor provided tests.", 0, 
				instance.getTests().size());
	}

	/**
	 * Test one of the parametrised constructors 
	 * ({@link Threadr#Threadr(List)). The constructor accepts a {@link List} 
	 * of {@link ParallelTask} objects. The test fails if Threadr contains an  
	 * incorrect number of tasks once instantiated.
	 */
	@Test
	public final void testTaskListConstructor() {
		
		List<ParallelTask> tasks = new ArrayList<>();
		
		for (int i = 0; i < REFERENCE_COUNT; i++) {
			tasks.add(new ReferenceTask());
		}
		
		instance = new Threadr(tasks);
		
		assertNotNull("Failed to instantiate Threadr.", instance);
		assertEquals("Incorrect number of tasks found.", REFERENCE_COUNT, 
				instance.getTasks().size());
		assertEquals("List constructor provided tests.", 0, 
				instance.getTests().size());
	}

	/**
	 * Test one of the parametrised constructors 
	 * ({@link Threadr#Threadr(ParallelTask[], ThreadTest[])))). The 
	 * constructor accepts two arrays, one containing {@link ParallelTask} 
	 * objects and the other containing {@link ThreadrTest} objects. The test 
	 * fails if Threadr contains an incorrect number of tasks or tests once  
	 * instantiated.
	 */
	@Test
	public final void testTaskTestArrayConstructor() {
		
		ParallelTask[] tasks = new ParallelTask[REFERENCE_COUNT];
		ThreadTest[] tests = new ThreadTest[REFERENCE_COUNT];
		
		for (int i = 0; i < REFERENCE_COUNT; i++) {
			tasks[i] = new ReferenceTask();
			tests[i] = new ReferenceTest();
		}
		
		instance = new Threadr(tasks, tests);
		
		assertNotNull("Failed to instantiate Threadr.", instance);
		assertEquals("Incorrect number of tasks found.", REFERENCE_COUNT, 
				instance.getTasks().size());
		assertEquals("Incorrect number of tests found.", REFERENCE_COUNT, 
				instance.getTests().size());
	}
	
	/**
	 * Test one of the parametrised constructors 
	 * ({@link Threadr#Threadr(List, List))). The constructor accepts two 
	 * {@link List} objects, one containing {@link ParallelTask} objects and 
	 * the other containing {@link ThreadrTest} objects. The test fails if 
	 * Threadr contains an incorrect number of tasks or tests once 
	 * instantiated.
	 */
	@Test
	public final void testTaskTestListConstructor() {
		
		List<ParallelTask> tasks = new ArrayList<>();
		List<ThreadTest> tests = new ArrayList<>();
		
		for (int i = 0; i < REFERENCE_COUNT; i++) {
			tasks.add(new ReferenceTask());
			tests.add(new ReferenceTest());
		}
		
		instance = new Threadr(tasks, tests);
		
		assertNotNull("Failed to instantiate Threadr.", instance);
		assertEquals("Incorrect number of tasks found.", REFERENCE_COUNT, 
				instance.getTasks().size());
		assertEquals("Incorrect number of tests found.", REFERENCE_COUNT, 
				instance.getTests().size());
	}
	
	/**
	 * Tests the {@link Threadr#addTask(ParallelTask) method to ensure it adds 
	 * a single {@link ParallelTask} to the framework properly. The test will 
	 * fail if no tasks are returned of an incorrect number of tasks is 
	 * returned.
	 */
	@Test
	public final void testAddTask() {
		
		instance = new Threadr();		
		ParallelTask task = new ReferenceTask();
		instance.addTask(task);
		
		assertNotNull("Instance returned null tasks.", instance.getTasks());
		assertEquals("Incorrect number of tasks found.", 1, 
				instance.getTasks().size());
	}
	
	/**
	 * Tests the {@link Threadr#addTask(ParallelTask, ThreadSafety)} method to 
	 * make sure that a {@link ParallelTask} can have a {@link ThreadSafety} 
	 * assigned to it whilst adding it to the framework. The test will fail if 
	 * no tasks or results are returned, or an incorrect number of task or 
	 * results is returned.
	 */
	@Test
	public final void testAddTaskTestResult() {
		
		instance = new Threadr();		
		ParallelTask task = new ReferenceTask();
		instance.addTask(task, ThreadSafety.THREAD);
		
		assertNotNull("Instance returned null tasks.", instance.getTasks());
		assertNotNull("Instance returned null results.", 
				instance.getThreadSafetyResults());
		assertEquals("Incorrect number of tasks found.", 1, 
				instance.getTasks().size());
		assertEquals("Incorrect number of results found.", 1, 
				instance.getThreadSafetyResults().size());
	}

	/**
	 * Tests the method for adding {@link ParallelTask} objects using a 
	 * {@link List}. The test fails if there is an incorrect number of tasks 
	 * stored.
	 */
	@Test
	public final void testAddTasksList() {
		
		instance = new Threadr();		
		List<ParallelTask> tasks = new ArrayList<>();
		
		for (int i = 0; i < REFERENCE_COUNT; i++) {
			tasks.add(new ReferenceTask());
		}
		instance.addTasks(tasks);
		
		assertNotNull("Instance returned null tasks.", instance.getTasks());
		assertEquals("Incorrect number of tasks found.", REFERENCE_COUNT, 
				instance.getTasks().size());
	}

	/**
	 * Tests the method for adding {@link ParallelTask} objects using a array. 
	 * The test fails if there is an incorrect number of tasks stored.
	 */
	@Test
	public final void testAddTasksArray() {
		
		instance = new Threadr();
		ParallelTask[] tasks = new ParallelTask[REFERENCE_COUNT];
		
		for (int i = 0; i < REFERENCE_COUNT; i++) {
			tasks[i] = new ReferenceTask();
		}
		instance.addTasks(tasks);
		
		assertNotNull("Instance returned null tasks.", instance.getTasks());
		assertEquals("Incorrect number of tasks found.", REFERENCE_COUNT, 
				instance.getTasks().size());
	}
	
	/**
	 * Tests the {@link Threadr#addTasks(Map)} method to make sure that 
	 * multiple {@link ParallelTask} and {@link ThreadSafety} pairs can be added 
	 * to the framework using a {@link Map}. The test will fail if the 
	 * framework returns null tests or null results. The test can also fail if 
	 * the framework does not overwrite the existing enties properly.
	 */
	@Test
	public final void testAddTasksTestResultsMap() {
		
		HashMap<ParallelTask, ThreadSafety> tasks = new HashMap<>();
		ParallelTask task1 = new ReferenceTask();
		tasks.put(task1, ThreadSafety.SEQUENTIAL);
		tasks.put(new ReferenceVMTask(), ThreadSafety.VM);
		tasks.put(new ReferenceUnsafeTask(), ThreadSafety.THREAD);
		
		instance = new Threadr();
		Map<? extends ParallelTask, ThreadSafety> old = instance.addTasks(tasks);
		assertNotNull("Null Map returned when empty Map expected.", old);
		assertTrue("Found existing safety results when none should exist.", 
				old.isEmpty());
		assertNotNull("Instance returned null tasks.", instance.getTasks());
		assertEquals("Incorrect number of tasks found.", 
				tasks.size(), instance.getTasks().size());
		assertEquals("Incorrect number of test results found.", 
				tasks.size(), instance.getThreadSafetyResults().size());
		
		tasks.put(task1, ThreadSafety.THREAD); // Update one entry.
		old = instance.addTasks(tasks);	
		/* Ensure the results are still roughly consistent. */
		assertNotNull("Null Map returned when populated Map expected.", old);
		assertFalse("Failed to find existing safety results when they exist.", 
				old.isEmpty());
		assertEquals(3, old.size());
		assertNotNull("Instance returned null tasks.", instance.getTasks());
		assertEquals("Incorrect number of tasks found.", 
				tasks.size(), instance.getTasks().size());
		assertEquals("Incorrect number of test results found.", 
				tasks.size(), instance.getThreadSafetyResults().size());
		/* Check the updated entry actually changed. */
		assertEquals(old.get(task1), ThreadSafety.SEQUENTIAL);
	}

	/**
	 * Tests the {@link Threadr#addTest(ThreadTest) method to ensure it adds 
	 * a single {@link ThreadTest} to the framework properly. The test will  
	 * fail if no tests are returned of an incorrect number of tests is 
	 * returned. 
	 */
	@Test
	public final void testAddTest() {
		
		ThreadTest test = new ReferenceTest();

		instance = new Threadr();
		instance.addTest(test);
		
		Set<? extends ThreadTest> tests = instance.getTests();
		assertNotNull("Stored tests are null.", tests);
		assertEquals("Incorrect number of tests found.", 1, tests.size());
	}
	
	/**
	 * Tests the method for adding {@link ThreadTest} objects using a  
	 * {@link List}. The test fails if there is an incorrect number of tasks 
	 * stored.
	 */
	@Test
	public final void testAddTestsList() {
		
		instance = new Threadr();		
		List<ThreadTest> tests = new ArrayList<>();
		
		for (int i = 0; i < REFERENCE_COUNT; i++) {
			tests.add(new ReferenceTest());
		}
		instance.addTests(tests);
		
		assertNotNull("Instance returned null tests.", instance.getTasks());
		assertEquals("Incorrect number of tests found.", REFERENCE_COUNT, 
				instance.getTests().size());
	}

	/**
	 * Tests the method for adding {@link ThreadTest} objects using a array. 
	 * The test fails if there is an incorrect number of tasks stored.
	 */
	@Test
	public final void testAddTestsArray() {
		
		instance = new Threadr();
		ThreadTest[] tests = new ThreadTest[REFERENCE_COUNT];
		
		for (int i = 0; i < REFERENCE_COUNT; i++) {
			tests[i] = new ReferenceTest();
		}
		instance.addTests(tests);
		
		assertNotNull("Instance returned null tests.", instance.getTests());
		assertEquals("Incorrect number of tests found.", REFERENCE_COUNT, 
				instance.getTests().size());
	}

	/**
	 * Tests the virtual machine parameter passing method. The test will fail 
	 * if Threadr returns incorrect stored parameters.
	 */
	@Test
	public final void testSetVMParameters() {
		
		instance = new Threadr();
		
		String[] params = {"param1", "param2", "param3"};
		instance.setVMParameters(params);
		
		String[] storedParams = instance.getVMParameters();
		
		assertNotNull("Instance returned null parameters.", storedParams);
		assertEquals("Returned parameter count incorrect.", params.length, 
				storedParams.length);
		assertArrayEquals("Returned parameters different.", params, 
				storedParams);
	}
	
	/**
	 * Tests the {@link Threadr#testSafety()} method to ensure that it returns 
	 * the correct thread safeness results when computing result, and ignores 
	 * pre-computed results when they are specified.
	 */
	@Test
	public final void testTestSafety() {
		
		/* Test it with just the tasks provided, no provided results. */
		testAddTasksArray();		
		instance.testSafety();
		Map<Class<?>, ThreadSafety> results = instance.getThreadSafetyResults();
		/* The results should exist and contain one entry. */
		assertNotNull("Returned safety results were null.", results);
		assertEquals("Incorrect number of results found.", 1, results.size());
		assertEquals(results.get(ReferenceTask.class), ThreadSafety.THREAD);
		
		/* Test the thread safety tester with provided safety values. */
		testAddTasksTestResultsMap();
		instance.addTask(new ReferenceTask(), null); // Force this to test.
		instance.testSafety();
		results = instance.getThreadSafetyResults();
		/* Ensure the results are there and the correct size first. */
		assertNotNull("Returned safety results were null.", results);
		assertEquals("Incorrect number of results found.", 3, results.size());
		
		/* This safety should be computed as it was null. */
		assertEquals("Incorrect thread safety found for ReferenceTask.", 
				results.get(ReferenceTask.class), ThreadSafety.THREAD);
		/* This safety is correct and provided, so it shouldn't change. */
		assertEquals("Incorrect thread safety found for ReferenceVMTask.", 
				results.get(ReferenceVMTask.class), ThreadSafety.VM);
		/* This is an incorrect safety, but it shouldn't be corrected. */
		assertEquals("Incorrect thread safety found for ReferenceUnsafeTask.",
				results.get(ReferenceUnsafeTask.class), ThreadSafety.THREAD);
	}

	/**
	 * Tests the main functionality of Threadr. Checks to see if results are 
	 * returned properly from the {@link Threadr#execute()} method.
	 */
	@Test
	public final void testExecute() {
		
		instance = new Threadr();
		ReferenceTask task = new ReferenceTask();
		ReferenceVMTask vmTask = new ReferenceVMTask();
		ReferenceUnsafeTask unsafeTask = new ReferenceUnsafeTask();
		/* Create copies of the arrays as a reference. */
		int[] refData = Arrays.copyOf(task.getData(), ReferenceTask.SIZE);
		int[] refVMData = Arrays.copyOf(vmTask.getData(), ReferenceTask.SIZE);
		int[] refUnsafeData = Arrays.copyOf(unsafeTask.getData(), 
				ReferenceTask.SIZE);
		
		List<ParallelTask> tasks = new ArrayList<>();
		tasks.add(task);
		tasks.add(vmTask);
		tasks.add(unsafeTask);
		
		instance.addTasks(tasks);
		instance.setSequentialHandler(new PostSequentialHandler());
		ThreadrResult results = instance.execute();
		/* Sort the arrays copied earlier. */
		Arrays.sort(refData);
		Arrays.sort(refVMData);
		Arrays.sort(refUnsafeData);
		
		assertNotNull("Returned null result.", results);
		/* Check the stored tasks and results are all there. */
		assertEquals("Result task count incorrect.", tasks.size(), 
				results.getTasks().size());
		assertEquals("Result tests count incorrect.", tasks.size(), 
				results.getResults().size());
		/* Ensure the arrays returned match the reference arrays. */
		assertArrayEquals("Threaded result incorrect.", refData, 
				(int[])results.getResult(task).getResult());
		assertArrayEquals("VM result incorrect.", refVMData, 
				(int[])results.getResult(vmTask).getResult());
		assertArrayEquals("Sequential result incorrect.", refUnsafeData, 
				(int[])results.getResult(unsafeTask).getResult());
	}

	/**
	 * Checks the {@link Threadr#getMemoryMonitor()} method to make sure 
	 * Threadr creates a new {@link MemoryMonitor} properly and that the 
	 * returned MemoryMonitor is for this process.
	 */
	@Test
	public final void testGetMemoryMonitor() {
				
		instance = new Threadr();
		MemoryMonitor monitor = instance.getMemoryMonitor();
		String pid = ManagementFactory.getRuntimeMXBean().getName();
		
		/* Make sure the MemoryMonitor for this process was returned. */
		assertNotNull("Returned MemoryMonitor was null.", monitor);
		assertEquals("Returned MemoryMonitor had different process ID.", 
				pid, monitor.getProcessID());
	}

	/**
	 * Checks the {@link Threadr#reset()} method to ensure it clears the tasks 
	 * and tests from Threadr properly.
	 */
	@Test
	public final void testReset() {
		
		/* Create an instance with tasks and tests. */
		testTaskTestListConstructor();
		
		instance.reset(); // Reset the instance.
		/* Make sure the tasks and tests are cleared properly. */
		assertEquals(0, instance.getTasks().size());
		assertEquals(0, instance.getTests().size());
	}
}
