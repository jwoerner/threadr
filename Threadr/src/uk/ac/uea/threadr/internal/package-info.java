/**
 * Internal classes for the Threadr framework. These do not provide anything of 
 * use to users, you should never need to use or instantiate anything found 
 * inside these packages.
 * 
 * @author Jordan Woerner
 */
package uk.ac.uea.threadr.internal;